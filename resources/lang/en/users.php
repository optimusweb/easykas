<?php 

return [
    'user'              => 'User',
    'users'             => 'Users',
    'name'              => 'Name',
    'email'             => 'Email',
    'avatar'            => 'Avatar',
    'avatar_example'    => 'Avatar example',
    'roles'             => 'Role',
    'password'          => 'Password',
    'confirm_password'  => 'Confirm Password',
    'category'          => 'category',
    'action'            => 'Action',
    'edit_user'         => 'Edit User',
    'create_user'       => 'Create User',
];