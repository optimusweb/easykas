<?php 

return [
	'roles'         => 'Roles',
    'role'          => 'Role',
    'description'   => 'Description',
    'action'        => 'Action',
    'edit_role'    => 'Edit Role',
    'create_role'  => 'Create Role',
    'permissions'	=> 'Permissions'
];