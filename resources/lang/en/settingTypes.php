<?php 

return [
    'settingType'       => 'Instellingstype',
    'settingTypes'      => 'Instellingstypen',
    'name'              => 'Naam',
    'value'             => 'Waarde',
    'action'            => 'Actie',
    'edit_setting'      => 'Instellingstype bewerken',
    'create_setting'    => 'Instellingstype aanmaken',
];