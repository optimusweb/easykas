<?php 

return [
    'permission'	=> 'Permission',
    'permissions'	=> 'Permissions',
    'name'          => 'Name',
    'display_name'  => 'Display name',
    'description'   => 'Description',
    'action'        => 'Action',
    'edit_permission'    => 'Edit permission',
    'create_permission'  => 'Create permission',
];