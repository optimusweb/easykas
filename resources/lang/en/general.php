<?php 

return [
    'app'   => [
        'name'                  => 'Aram Advies Kas',
        'welcome'               => 'Welcome',
        'general'               => 'General',
        'create_new'            => 'Create New',
        'confirm'               => [
            'delete' => [
                'title'             => 'Confirm Delete Record',
                'question'          => 'Are you sure you want to delete?',
                'value_to_delete'   => 'Value to delete:',
            ],
        ],
    ],
    'login' => [
        'title'                 => 'Login',
        'login'                 => 'Login',
        'email'                 => 'Email',
        'password'              => 'Password',
        'lost_your_password'    => 'Lost your password?',
    ],
        'logout' => [
        'logout' => 'Logout',
    ],
    'passwords' => [
        'title'                 => 'Reset Password',
        'email'                 => 'Email',
        'send_password_link'    => 'Send Password Reset Link',
    ],
    'form'  => [
        'create_record' => 'Create Record',
        'save_changes'  => 'Save Changes',
        'delete'        => 'Yes am sure. Delete', 
        'flash' => [
            'created'   => ':name record has successfully been created.',
            'updated'   => 'Changes to :name have successfully been saved.',
            'deleted'   => ':name has successfully been deleted.',
        ],
    ],
    'nav'   => [
        'back'              => 'Back',
        'home'              => 'Home',
        'products'          => 'Products',
        'brands_list'       => 'Brands List',
        'categories_list'   => 'Categories List',
        'products_list'     => 'Products List',
        'roles_list'        => 'Roles List',
        'permissions_list'  => 'Permissions List',
        'user_roles_list'   => 'User roles List',
        'customers'         => 'Customers',
        'orders'            => 'Orders',
        'users_list'        => 'Users List',
        'users'             => 'Users',
        'logout'            => 'Log Out',
    ],
];