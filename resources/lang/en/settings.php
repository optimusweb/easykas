<?php

return [
    'setting'           => 'Instelling',
    'settings'          => 'Instellingen',
    'name'              => 'Naam',
    'value'             => 'Waarde',
    'action'            => 'Actie',
    'edit_setting'      => 'Instelling bewerken',
    'create_setting'    => 'Instelling aanmaken',
];