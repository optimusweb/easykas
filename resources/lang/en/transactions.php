<?php 

return [
    'transaction'           => 'Mutatie',
    'transactions'          => 'Mutaties',
    'deleted_transactions'  => 'Verwijderde mutaties',
    'name'                  => 'Naam',
    'action'                => 'Actie',
    'edit_transaction'      => 'Mutatie bewerken',
    'create_transaction'    => 'Mutatie aanmaken',
];