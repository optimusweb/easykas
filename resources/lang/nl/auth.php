<?php 
   
return [
    'failed'    => 'De inloggegevens komen niet voor in ons systeem, of uw account is geblokkeerd.',
    'throttle'  => 'Te veel verkeerde inlogpogingen. Probeer het a.u.b. over :seconds seconden.',
];