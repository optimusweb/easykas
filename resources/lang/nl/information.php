<?php 

return [
    'information'             => 'Informatie',
    'information_icon'        => 'Informatie icoon',
    'information_page'        => 'Informatie pagina',
    'text'                    => 'Tekst',
    'edit_information_icon'   => 'Bewerk informatie icoon',
    'edit_information_page'   => 'Bewerk informatie pagina',
];