<?php 

return [
    'roles'         => 'Rollen',
    'role'          => 'Rol',
    'name'          => 'Naam',
    'display_name'  => 'Weergave naam',
    'description'   => 'Omschrijving',
    'permissions'   => 'Permissies',
    'action'        => 'Actie',
    'edit_role'     => 'Bewerk Rol',
    'create_role'   => 'Maak Rol',
    'permissions'	=> 'Permissies'
];