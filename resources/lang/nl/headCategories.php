<?php 

return [
    'head_category'          => 'Hoofdcategorie',
    'head_categories'        => 'Hoofdcategorieën',
    'deleted_head_categories'=> 'Verwijderde hoofdcategorieën',
    'name'                   => 'Naam',
    'action'                 => 'Actie',
    'edit_head_category'     => 'Hoofdcategorie bewerken',
    'create_head_category'   => 'Hoofdcategorie aanmaken',
];