<?php 

return [
    'settingType'       => 'Instellingstype',
    'settingTypes'      => 'Instellingstypen',
    'code'              => 'Code',
    'name'              => 'Naam',
    'value'             => 'Waarde',
    'action'            => 'Actie',
    'edit_settingType'  => 'Instellingstype bewerken',
    'create_settingType'=> 'Instellingstype aanmaken',
];