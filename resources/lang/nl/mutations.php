<?php 

return [
    'mutation'              => 'Mutatie',
    'mutations'             => 'Mutaties',
    'select_customer'       => 'Selecteer klant',
    'select_period'         => 'Selecteer periode',
    'from'                  => 'Van',
    'to'                    => 'Tot',
    'booking_year'          => 'Boekjaar',
    'period'                => 'Periode',
    'start_balance'         => 'Begin saldo',
    'btw_code'              => 'BTW-co',
    'btw_percent'           => 'BTW-%',
    'btw_amount'            => 'BTW-bedrag',
    'cost_description'      => 'Kostpl-omschr.',
    'beginsaldo'            => 'Beginsaldo',
];