<?php 

return [
    'category'          => 'Categorie',
    'categories'        => 'Categorieën',
    'deleted_categories'=> 'Verwijderde categorieën',
    'name'              => 'Naam',
    'action'            => 'Actie',
    'use_receipts'      => 'Koppel bonnen',
    'is_negative'       => 'Is negatief getal',
    'edit_category'     => 'Categorie bewerken',
    'create_category'   => 'Categorie aanmaken',
];