<?php 

return [
    'permission'	=> 'Permissie',
    'permissions'	=> 'Permissies',
    'name'          => 'Naam',
    'display_name'  => 'Weergave naam',
    'description'   => 'Description',
    'action'        => 'Action',
    'edit_permission'    => 'Bewerk permissie',
    'create_permission'  => 'Nieuwe permissie',
];