<?php 

return [
    'bookingType'       => 'Boekingstype',
    'bookingTypes'      => 'Boekingstypen',
    'deleted_bookingTypes'=> 'Verwijderde boekingstypen',
    'code'              => 'Code',
    'name'              => 'Naam',
    'description'       => 'Omschrijving',
    'gb_rek'            => 'GB-rek.',
    'btw_code'          => 'BTW code',
    'default'           => 'Standaard',
    'action'            => 'Actie',
    'edit_bookingType'  => 'Bewerk boekingstype',
    'create_bookingType'=> 'Maak aan boekingstype',
    'deactivated_bookingTypes' => 'Ge-deactiveerde Boekingstypen',
];