<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('general.app.name')</title>

    <link rel="shortcut icon" type="image/png" href="{{asset('admin/images/favicon.png')}}"/>
    <!-- Bootstrap -->
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('admin/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('admin/css/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('admin/css/green.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset('admin/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset('admin/css/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- Custom Theme Style -->
    <link href="{{asset('admin/css/custom.min.css')}}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/bootsnipp.css')}}" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title text-center mt-3" style="border: 0;">
                    {{--<a href="#" class="site_title"><i class="fa fa-bar-chart"></i> <span>@lang('general.app.name')</span></a>--}}
                    <img src="{{ asset("admin/images/logo.png") }}" width="100px">
                </div>

                <div class="clearfix"></div>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        {{--<h3>@lang('general.app.general')</h3>--}}
                        <ul class="nav side-menu">
                            <li><a hef="#"><i class="fa fa-home"></i> @lang('general.nav.home') </a></li>
                            {{--@permission(('users'))--}}
                            <li><a><i class="fa fa-users"></i> @lang('general.nav.users') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('users.newRegistrations')}}">@lang('general.nav.new_registrations')</a></li>
                                    <li><a href="{{route('users.index')}}">@lang('general.nav.users_list')</a></li>
                                    <li><a href="{{route('roles.index')}}">@lang('general.nav.roles_list')</a></li>
                                    <li><a href="{{route('permissions.index')}}">@lang('general.nav.permissions_list')</a></li>
                                </ul>
                            </li>
                            {{--@endpermission--}}
                            <li><a><i class="fa fa-th"></i> @lang('general.nav.categories') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('headCategories.index')}}">@lang('general.nav.head_categories')</a></li>
                                    <li><a href="{{route('categories.index')}}">@lang('general.nav.categories')</a></li>
                                    <li><a href="{{route('categories.link')}}">@lang('general.nav.link_categories')</a></li>
                                    <li><a href="{{route('bookingTypes.index')}}">@lang('general.nav.bookings')</a></li>
                                    <li><a href="{{route('bookingTypes.link')}}">@lang('general.nav.link_bookings')</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-cogs"></i> @lang('settings.settings') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('settingTypes.index')}}">@lang('general.nav.setting_types')</a></li>
                                    <li><a href="{{route('settings.index')}}">@lang('general.nav.general')</a></li>
                                    <li><a href="{{route('home.index')}}">@lang('general.nav.information_icon')</a></li>
                                    <li><a href="{{route('home.index')}}">@lang('general.nav.general_others')</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-bars"></i> @lang('general.nav.overviews') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('home.index')}}">@lang('general.nav.quarters')</a></li>
                                    <li><a href="{{route('home.index')}}">@lang('general.nav.mutations')</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                @if(Auth::check())<img src=" {{ Auth::user()->getLogoUrl() }}" alt="">{{Auth::user()->firstName}} @if(Auth::user()->middleName) {{ Auth::user()->middleName }} @endif {{ Auth::user()->lastName }}@endif

                                <span class="fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li>
                                    <a href="{{route('changepass')}}" ><i class="fa fa-lock pull-right"></i> @lang('users.change_password')</a>
                                </li>
                                <li>
                                    {{--<a href="{{route('logout')}}"--}}
                                    {{--onclick="event.preventDefault();--}}
                                    {{--document.getElementById('logout-form').submit();">--}}
                                    <a href="{{route('logout')}}" ><i class="fa fa-sign-out pull-right"></i> @lang('general.logout.logout')</a>

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}

                                </li>

                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @include('templates.admin.partials.alerts')
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <p>Optimus Websolutions © 2017 All Rights Reserved.</p>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="{{asset('admin/js/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/js/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('admin/js/nprogress.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('admin/js/icheck.min.js')}}"></script>
<!-- Datatables -->
<script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('admin/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/js/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('admin/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('admin/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('admin/js/datatables.scroller.min.js')}}"></script>
<script src="{{asset('admin/js/jszip.min.js')}}"></script>
<script src="{{asset('admin/js/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/js/vfs_fonts.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('admin/js/custom.min.js')}}"></script>

<!-- Datatables -->
<script>
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
//                    {
//                        extend: "copy",
//                        className: "btn-sm"
//                    },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    language: {
                        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"
                    },
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[ 1, 'asc' ]],
            'columnDefs': [
                { orderable: false, targets: [0] }
            ]
        });
        $datatable.on('draw.dt', function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

        // Multitab functions
        $('#multiTab a[href="#account"]').tab('show');

        $('#multiTab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })

        // error in tab
        tabid = "";
        $(".nav-link").each(function( index ) {
            $(".has-error" ).each(function( index ) {
                tabid = $(this).parent().attr("id");
            });
            if ($(this).attr("href") == "#"+tabid){
                $(this).css("color", "#a94442");
            }
        });

        if (tabid != ""){
            $(".tab-content").after('<div class="form-group has-error">'+
                '<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">'+
                '<span class="help-block">@lang('general.error.tab_error')</span>'+
                '</div>'+
                '<div>');

        }

    });
</script>
<!-- /Datatables -->
</body>
</html>