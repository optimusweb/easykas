<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('general.app.name')</title>

    <link rel="shortcut icon" type="image/png" href="{{asset('admin/images/favicon.png')}}"/>
    <!-- Bootstrap -->
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('admin/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('admin/css/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('admin/css/green.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset('admin/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset('admin/css/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- Custom Theme Style -->
    <link href="{{asset('admin/css/custom.min.css')}}" rel="stylesheet">
    <!-- Datatables -->
{{--    <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">--}}
{{--    <link href="{{asset('admin/js/datatables/dataTables.css')}}" rel="stylesheet">--}}
    <link href="{{asset('admin/js/datatables/DataTables-1.10.16/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/js/datatables/Buttons-1.5.1/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/js/datatables/FixedHeader-3.1.3/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/js/datatables/Responsive-2.2.1/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/js/datatables/Scroller-1.4.4/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/bootsnipp.css')}}" rel="stylesheet">
    <link href="{{asset('admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title text-center mt-3" style="border: 0;">
                    <div class="logo"></div>
                </div>

                <div class="clearfix"></div>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        {{--<h3>@lang('general.app.general')</h3>--}}
                        <ul class="nav side-menu">
                            <li><a hef="#"><i class="fa fa-home"></i> @lang('general.nav.home') </a></li>
                            @permission(('users'))
                            <li><a><i class="fa fa-users"></i> @lang('general.nav.users') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('users.newRegistrations')}}">@lang('general.nav.new_registrations')</a></li>
                                    <li><a href="{{route('users.index_app')}}">@lang('general.nav.users_app_list')</a></li>
                                    <li><a href="{{route('users.index')}}">@lang('general.nav.users_list')</a></li>
                                    <li><a href="{{route('roles.index')}}">@lang('general.nav.roles_list')</a></li>
                                    <li><a href="{{route('permissions.index')}}">@lang('general.nav.permissions_list')</a></li>
                                </ul>
                            </li>
                            @endpermission
                            @permission(('categories'))
                            <li><a><i class="fa fa-th"></i> @lang('general.nav.categories') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('headCategories.index')}}">@lang('general.nav.head_categories')</a></li>
                                    <li><a href="{{route('categories.index')}}">@lang('general.nav.categories')</a></li>
                                    <li><a href="{{route('categories.link')}}">@lang('general.nav.link_categories')</a></li>
                                    <li><a href="{{route('bookingTypes.index')}}">@lang('general.nav.booking_types')</a></li>
                                    <li><a href="{{route('bookingTypes.link')}}">@lang('general.nav.link_bookings')</a></li>
                                </ul>
                            </li>
                            @endpermission
                            @permission(('settings'))
                            <li><a><i class="fa fa-cogs"></i> @lang('settings.settings') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('settingTypes.index')}}">@lang('general.nav.setting_types')</a></li>
                                    <li><a href="{{route('settings.index')}}">@lang('general.nav.general')</a></li>
                                    <li><a href="{{route('information.index')}}">@lang('general.nav.information_icon')</a></li>
                                    <li><a href="{{route('information.information_page')}}">@lang('general.nav.information_page')</a></li>
                                </ul>
                            </li>
                            @endpermission
                            @permission(('transactions'))
                            <li><a><i class="fa fa-file-text"></i> @lang('general.nav.transactions') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('transactions.index')}}">@lang('general.nav.transactions')</a></li>
                                </ul>
                            </li>
                            @endpermission
                            @permission(('overviews'))
                            <li><a><i class="fa fa-bars"></i> @lang('general.nav.overviews') <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('transactions.mutation_quarters')}}">@lang('general.nav.quarters')</a></li>
                                    <li><a href="{{route('transactions.mutations')}}">@lang('general.nav.mutations')</a></li>
                                </ul>
                            </li>
                            @endpermission
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <div class="text-center">v1.0</div>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                @if(Auth::check())@if(!empty(Auth::user()->getLogoUrl()))<img src=" {{ Auth::user()->getLogoUrl() }}" alt="">@endif{{Auth::user()->firstName}} @if(Auth::user()->middleName) {{ Auth::user()->middleName }} @endif {{ Auth::user()->lastName }}@endif
                                <span class="fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li>
                                    <a href="{{route('changepass')}}" ><i class="fa fa-lock pull-right"></i> @lang('users.change_password')</a>
                                </li>
                                <li>
                                    {{--<a href="{{route('logout')}}"--}}
                                    {{--onclick="event.preventDefault();--}}
                                    {{--document.getElementById('logout-form').submit();">--}}
                                    <a href="{{route('logout')}}" ><i class="fa fa-sign-out pull-right"></i> @lang('general.logout.logout')</a>

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}

                                </li>

                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @include('templates.admin.partials.alerts')
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer class="footer_fixed">
            {{--<div class="pull-right">--}}
                {{--<p>Optimus Websolutions © 2017 All Rights Reserved.</p>--}}
            {{--</div>--}}
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="{{asset('admin/js/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/js/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('admin/js/nprogress.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('admin/js/icheck.min.js')}}"></script>
<!-- Datatables -->
<script src="{{asset('admin/js/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/DataTables-1.10.16/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Buttons-1.5.1/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Buttons-1.5.1/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Buttons-1.5.1/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Buttons-1.5.1/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Buttons-1.5.1/js/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/FixedHeader-3.1.3/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/KeyTable-2.3.2/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Responsive-2.2.1/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/Responsive-2.2.1/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('admin/js/datatables/Scroller-1.4.4/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/js/datatables/pdfmake-0.1.32/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/js/moment/moment.js')}}"></script>
<script src="{{asset('admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('admin/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{asset('admin/js/custom.min.js')}}"></script>

<!-- Datatables -->
<script>
    $(document).ready(function() {
        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "csv",
                            className: "btn-sm",
                            exportOptions: {
                                columns: ':not(.action)'
                            }
                        },
                        {
                            extend: "excel",
                            className: "btn-sm",
                            exportOptions: {
                                columns: ':not(.action)'
                            }
                        },
                        {
                            extend: "pdf",
                            className: "btn-sm",
                            customize: function (doc) {
                                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                doc.defaultStyle.fontSize = 9; //2,3,4,etc
                                doc.styles.tableHeader.fontSize = 9;
                                doc.styles.tableHeader.alignment = 'left';
                            },
                            exportOptions: {
                                columns: ':not(.action)'
                            }
                        },
                        {
                            extend: "print",
                            className: "btn-sm",
                            exportOptions: {
                                columns: ':not(.action)'
                            }
                        },
                    ],
                    language: {
                        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"
                    },
                    responsive: true,
                    pageLength: 20,
                });

            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable({
            language: {
                url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"
            },
            responsive: true,
            pageLength: 20,
            paging: false,
            info: false,
        });

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        $('#datatable-clean').DataTable({
            paging: false,
            ordering: false,
            info: false,
            searching: false
        });

        $('#datatable-no-search').DataTable({
            searching: false,
            dom: "Bfrtip",
            buttons: [
                {
                    extend: "csv",
                    className: "btn-sm",
                    exportOptions: {
                        columns: ':not(.action)'
                    },
                    header: false
                },
                {
                    extend: "excel",
                    className: "btn-sm",
                    exportOptions: {
                        columns: ':not(.action)'
                    },
                    header: false,
                    title: null
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
//                    footer: true,
                    pageSize: 'A4',
                    className: "btn-sm",
                    exportOptions: {
                        columns: ':not(.hidden)'
                    },
                    customize: function (doc) {
                        doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.defaultStyle.fontSize = 9;
                        doc.styles.tableHeader.fontSize = 9;
                        doc.styles.tableHeader.alignment = 'left';
                        doc.styles.tableHeader.margins = [10, 10, 10, 10];
                    }
                },
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"
            },
            pageLength: 20,
//            ordering: false,
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            order: [[1, 'asc']],
            columnDefs: [
                {
                    orderable: false,
                    width: '50px',
                    targets: [0]
                }
            ],
            paging: false,
            ordering: false,
            info: false,
            searching: false,
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

        // Multitab functions
        $('#multiTab a[href="#account"]').tab('show');

        $('#multiTab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })

        // error in tab
        tabid = "";
        $(".nav-link").each(function (index) {
            $(".has-error").each(function (index) {
                tabid = $(this).parent().attr("id");
            });
            if ($(this).attr("href") == "#" + tabid) {
                $(this).css("color", "#a94442");
            }
        });

        if (tabid != "") {
            $(".tab-content").after('<div class="form-group has-error">' +
                '<div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">' +
                '<span class="help-block">@lang('general.error.tab_error')</span>' +
                '</div>' +
                '<div>');

        }

        // hide alert-success after 5 seconds
        $(".alert-success").slideUp(600).delay(4000).fadeOut(800);

        /*
         transactions create, change url for choosen user
         */
        $("#transactions #user_id").on("change", function () {
            var url = window.location.href;
            var str = url.substr(url.lastIndexOf('/') + 1) + '$';
            window.location.href = url.replace(new RegExp(str), '') + $(this).val();
        });

        /*
         Count all the bookingtypes values for each category
         */
        $("div[id^='cat_']").each(function () {
            var cat_div = $(this).attr("id");
            // get the value which is set in the id
            var cat_id = cat_div.split('_').pop().trim();
            $("#cat_block_" + cat_id + " :text").on("keyup", function () {
                if (this.value != '' && $.isNumeric(this.value) === false && $(this).attr("id").indexOf("receipt_desc") === -1) {
//                    $(this).parents(".form-group").addClass("has-error");
                    $(this).parent().addClass("has-error");
                    $(this).parent().siblings('span').removeClass('hidden');
                } else {
                    $(this).parent().removeClass("has-error");
                    $(this).parent().siblings('span').addClass('hidden');
                    if ($(this).attr("id").indexOf("receipt_desc") === -1) {
                        // count all input values for the given category group
                        var sum = 0;
                        $("#cat_block_" + cat_id + " :text").each(function () {
                            if ($(this).attr("id").indexOf("receipt_desc") === -1) {
                                sum += Number($(this).val());
                            }
                        });

                        // set the total count to div
                        $("#show_total_cat_" + cat_id).text(sum);
                        $("#cat_" + cat_id).val(sum);
                    }
                }
                // automatic count the kasmutatie value
                var cat1 = 0;
                var cat2 = 0;
                var cat3 = 0;

                if ($("#cat_1").val() != null && $("#cat_1").val() != '') {
                    var cat1 = parseInt($("#cat_1").val());
                }
                if ($("#cat_2").val() != null && $("#cat_2").val() != '') {
                    var cat2 = parseInt($("#cat_2").val());
                }
                if ($("#cat_3").val() != null && $("#cat_3").val() != '') {
                    var cat3 = parseInt($("#cat_3").val());
                }

                $(".kasmutatie input").val(cat1 + cat2 + cat3);
            });
        });

        // add errors at the fields in transaction with wrong input
        $("div[id^='bt_block_']").each(function () {
            var bt_div = $(this).attr("id");
//            // get the value which is set in the id
//            var bt_div = bt_div.split('_').pop().trim();
            $("#" + bt_div + " :text").on("keyup", function () {
                if (this.value != '' && $.isNumeric(this.value) === false) {
                    $(this).parents(".form-group").addClass("has-error");
                    $(this).parent().siblings('span').removeClass('hidden');
                } else {
                    $(this).parents(".form-group").removeClass("has-error");
                    $(this).parent().siblings('span').addClass('hidden');
                }
//                    if ($(this).attr("id") != "receipt_desc[]") {
//                        // count all input values for the given category group
//                        var sum = 0;
//                        $("#cat_block_" + cat_id + " :text").each(function () {
//                            sum += Number($(this).val());
//                        });
//
//                        // set the total count to div
//                        $("#show_total_cat_" + cat_id).text(sum);
//                        $("#cat_" + cat_id).val(sum);
//                    }
//                }
            });
        });

        // count the kasverschil by adding the field kasgeld and kasmutatie
        $(".kasgeld input").on("keyup", function () {
            if ($(this).val() != null) {
                $(".kasverschil input").val(parseInt($(".kasmutatie input").val()) - parseInt($(this).val()));
            }
        });

        // count the kassaldo eind with the values of kassaldo begin + telling kasgeld + result of opnemen/storten
//        $(".kasgeld input").on("keyup", function(){
//            if ($(this).val() != null) {
//                $(".kasverschil input").val(parseInt($(this).val()) + parseInt($(".kasmutatie input").val()));
//            }
//        });

        /*
         BookingTypes copy and replace name text with _ for code value
         */
        if (!$('#code').is(':disabled')) {
            $("#name").on("keyup", function () {
                var text = $(this).val();
                text = text.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '_').toLowerCase();
                $("#code_show").val(text);
                $("#code").val(text);
            });
        }

        // load headcategory_tree when Category is changed for user
        var path = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
        $('#head_category_id').bind('change load_now', function () {
            var id = $(this).val();
            if (id == "") {
                $("#categories_tree").html("<label>Boekingstypen waarden worden hierin weergegeven. Selecteer eerst een Categorie links.</label>");
            } else {
                $.ajax({
                    type: 'POST',
                    url: path + '/api/login',
                    dataType: 'json',
                    async: true,
                    data: {
                        email: 'api@aramadvies.nl',
                        password: 'Api132',
                    },

                    success: function (response) {
                        getCategories(response, id);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }).triggerHandler("load_now");

        function getCategories(response, id) {
            var result = JSON.parse(JSON.stringify(response.success));
            var token = result.token;
            var userId = $('#user_id').val();
            {{--var remember_token = {{Auth::user()->remember_token}};--}}

            $.ajax({
                type: 'GET',
                url: path + '/api/categories_tree/' + userId,
                dataType: 'json',
//                headers: {
//                    'Authorization': 'Bearer ' + token,
//                },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var tree = JSON.parse(JSON.stringify(response.categories_tree));
                    var result = JSON.parse(JSON.stringify(response.categories_tree.head_category));
                    var html = "";
                    $.each(result, function (idx, headCats) {
                        $.each(headCats.categories, function (idx, val) {
                            html += "<div class='form-group'>" +
                                    "   <label>" + val.name + "</label>" +
                                    "</div>" +
                                    "<div class='form-group'>" +
                                    "   <div class='col-md-5'>" +
                                    "       <label>Naam</label>" +
                                    "   </div>" +
                                    "   <div class='col-md-5'>" +
                                    "       <label>Omschrijving</label>" +
                                    "   </div>" +
                                    "   <div class='col-md-2'>" +
                                    "       <label>Gb-rek</label>" +
                                    "   </div>" +
                                    "</div>";
                            $.each(val.booking_types, function (idx, bt) {
                                var ubt_name = "";
                                var ubt_descr = "";
                                var ubt_gb_rek = "";
                                var ubt_user_id = "";

    //                            if ($.inArray('4', bt.user_booking_types.map(function(x) {
    //                                    console.log(" array user_id: ",  x.user_id);
    //                                })) != -1);

                                $.each(bt.user_booking_types, function (idx, ubt) {
                                    if (ubt != null && ubt.user_id == userId) {
//                                      console.log("ubt_user_id: ", ubt);
//                                      if (bt.user_booking_types != null) {
                                        ubt_name = ubt.name;
                                        ubt_descr = ubt.description;
                                        ubt_gb_rek = ubt.gb_rek;


                                    }
                                });
                                        html += "<div class='form-group'>" +
                                            "   <div class='col-md-5'>" +
                                            "      <input type='text' value='" + ubt_name + "' name='bt_name_" + bt.id + "' class='form-control' placeholder='" + bt.name + "'>" +
                                            "   </div>" +
                                            "   <div class='col-md-5'>" +
                                            "      <input type='text' value='" + ubt_descr + "' name='bt_descr_" + bt.id + "' class='form-control' placeholder='" + bt.name + "'>" +
                                            "   </div>" +
                                            "   <div class='col-md-2'>" +
                                            "      <input type='text' value='" + ubt_gb_rek + "' name='bt_gb_rek_" + bt.id + "' class='form-control' placeholder='" + bt.gb_rek + "'>" +
                                            "   </div>" +
                                            "</div>";
    //                                }

                            });
                        });
                    });
                    $("#categories_tree").html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        // Date Picker
        $.fn.datepicker.dates['nl'] = {
            days: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
            daysShort: ["zo", "ma", "di", "wo", "do", "vr", "za"],
            daysMin: ["zo", "ma", "di", "wo", "do", "vr", "za"],
            months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
            monthsShort: ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"],
            today: "Vandaag",
            monthsTitle: "Maanden",
            clear: "Wissen",
            weekStart: 1,
            format: "dd-mm-yyyy"
        };
        jQuery('#datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            language: 'nl'
        });

        // daterangepicker with all options
        var startDate = '';
        var endDate = '';
        @if(isset($dateRanges) && !empty($dateRanges))
            startDate = '{{$startDate}}';
        endDate = '{{$endDate}}';
        @else
            startDate = moment().subtract(29, 'days').format('DD-MM-YYYY');
        endDate = moment().format('DD-MM-YYYY');
        @endif
        $('#reportrange span').html(startDate + ' t/m ' + endDate);
        $('#date_range').val(startDate + '/' + endDate);

        moment.locale('nl');
        $('#reportrange').daterangepicker({
            format: 'dd-mm-yyyy',
            startDate: startDate, //moment().subtract(29, 'days'),
            endDate: endDate, //moment(),
            minDate: '01-01-2017',
            maxDate: '31-12-2050',
            dateLimit: {
                days: 120
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Vandaag': [moment(), moment()],
                'Gisteren': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Laatste 7 Dagen': [moment().subtract(6, 'days'), moment()],
                'Laatste 30 Dagen': [moment().subtract(29, 'days'), moment()],
                'Deze maand': [moment().startOf('month'), moment().endOf('month')],
                'Afgelopen maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-success',
            cancelClass: 'btn-default',
            separator: ' tot ',
            locale: {
                format: 'DD-MM-YYYY',
                applyLabel: 'Bevestig',
                cancelLabel: 'Annuleer',
                fromLabel: 'Van',
                toLabel: 'Tot',
                customRangeLabel: 'Handmatig',
                daysOfWeek: ['Zo', 'Ma', 'Di', 'Woe', 'Do', 'Vr', 'Za'],
                monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
                firstDay: 1
            }
        }, function (start, end, label) {
//            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('DD-MM-YYYY') + ' t/m ' + end.format('DD-MM-YYYY'));
            $('#date_range').val(start.format('DD-MM-YYYY') + '/' + end.format('DD-MM-YYYY'));
        });

        if ($(".text-icon").length){
            var count = 250;
            // text-icon field max 250 characters
            // count the existing karakters and substract it from the count val
            $(".text_icon_counter").text(count - $(".text-icon").val().length);
            // substract every typed in character from count
            $('.text-icon').on("keyup", function () {
                var tlength = $(this).val().length;
                $(this).val($(this).val().substring(0, count));
                var tlength = $(this).val().length;
                remain = count - parseInt(tlength);
                $('.text_icon_counter').text(remain);
            });
        }
    });
</script>
<!-- /Datatables -->
</body>
</html>