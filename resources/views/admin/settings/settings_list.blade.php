@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <tr class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('settings.settings') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('settings.edit', ['id' => 1])}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> @lang('general.app.edit') </a>
                        </div>
                    </div>
                </div>
                <td class="x_content">
                    <table id="datatable-clean" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('settings.setting')</th>
                                <th>@lang('settings.value')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($settingTypes as $type)
                            <tr>
                                <td>{{ $type->name }}</td>
                                <td>@if(isset($type->settings->value )){{ $type->settings->value }}@else - @endif</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop