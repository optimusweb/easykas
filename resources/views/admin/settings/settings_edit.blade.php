@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <h2>@lang('settingTypes.edit_settingType')</h2>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                            <a href="{{route('settings.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" action="{{ route('settings.update', ['id' => 1]) }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        @foreach($settingTypes as $type)
                            <div class="form-group row {{ $errors->has($type->code) ? ' has-error' : '' }}">
                                <label class="col-sm-3 col-form-label col-xs-12" for="{{ $type->code }}">{{ $type->name }} <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <?php if(isset($type->settings->value)){ $val = $type->settings->value; }else{ $val = ''; } ?>
                                    <input type="text" value="{{ old($type->code) ?: $val }}" id="{{ $type->code }}" name="{{ $type->code }}" class="form-control">
                                    @if ($errors->has($type->code))
                                        <span class="help-block">{{ $errors->first($type->code) }}</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-success">@lang('general.form.save_changes')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-script')
    <script type="text/javascript">
        $(document).ready(function()
        {
            show_fields();

            $("#period").on("change", function()
            {
                show_fields();
            });
        });

        function show_fields(){
            switch ($("#period").val()) {
                case '1':
                    $("#day_month_block").show();
                    $("#start_date_block").hide();
                    $("#number_terms_block").hide();
                    break;
                case '2':
                case '3':
                    $("#day_month_block").hide();
                    $("#start_date_block").show();
                    $("#number_terms_block").hide();
                    break;
                case '4':
                    $("#number_terms_block").show();
                    $("#start_date_block").show();
                    $("#day_month_block").hide();
                default:
                    break;
            }
        }
    </script>
@stop