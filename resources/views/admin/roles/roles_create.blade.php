@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('roles.create_role') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('roles.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" action="{{ route('roles.store') }}" data-parsley-validate class="form-horizontal form-label-left">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="role">@lang('roles.name') <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ Request::old('name') ?: '' }}" id="name" name="name" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="role">@lang('roles.display_name') <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ Request::old('display_name') ?: '' }}" id="display_name" name="display_name" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('display_name'))
                                <span class="help-block">{{ $errors->first('display_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="description">@lang('roles.description') <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{ Request::old('description') ?: '' }}" id="description" name="description" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('description'))
                                <span class="help-block">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="permissions">@lang('roles.permissions') <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well" style="max-height: 300px;overflow: auto;">
                                    @foreach($permissions as $permission)
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox{{ $permission->id }}" type="checkbox" name="permissions[]" value="{{ $permission->id }}" @if (old('permissions') == $permission->id)) checked="" @endif >
                                            <label for="checkbox{{ $permission->id }}">
                                                {{ $permission->display_name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                @if ($errors->has('permissions'))
                                    <span class="help-block">{{ $errors->first('permissions') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop