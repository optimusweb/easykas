@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('roles.roles') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('roles.create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="roles table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('roles.role')</th>
                                <th>@lang('roles.description')</th>
                                <th>@lang('roles.permissions')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('roles.action')</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('roles.role')</th>
                                <th>@lang('roles.description')</th>
                                <th>@lang('roles.permissions')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('roles.action')</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($roles))
                            {{-- {{ dd($roles) }} --}}
                            @foreach($roles as $row)
                            <tr>
                                <td>{{$row->display_name}}</td>
                                <td>{{$row->description}}</td>
                                <td>
                                    @foreach($row->permissions as $permission){{ $permission->name }}@if ($row->permissions->last() !== $permission), @endif @endforeach
                                </td>
                                @ability('','edit,delete')
                                <td class="action">
                                    @permission(('edit'))
                                    <a href="{{ route('roles.edit', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                    @endpermission
                                    @permission(('delete'))
                                    <a href="{{ route('roles.show', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="Delete"></i> </a>
                                    @endpermission
                                </td>
                                @endability
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop