@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('general.app.confirm.delete.title') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('categories.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <p>@lang('general.app.confirm.delete.question') <strong>{{$headCategory->name}}</strong></p>

                    <form method="POST" action="{{ route('categories.destroy_link', ['id' => $headCategory->id ]) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-danger">@lang('general.form.delete') <strong>{{$headCategory->name}}</strong></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop