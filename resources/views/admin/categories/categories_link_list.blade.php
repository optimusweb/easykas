@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('categories.categories') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('categories.create_link')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_link') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>@lang('headCategories.head_category')</th>
                            <th>@lang('categories.categories')</th>
                            @ability('','edit,delete')
                            <th class="action">@lang('categories.action')</th>
                            @endability
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>@lang('headCategories.head_category')</th>
                            <th>@lang('categories.categories')</th>
                            @ability('','edit,delete')
                            <th>@lang('categories.action')</th>
                            @endability
                        </tr>
                        </tfoot>
                        <tbody>
                        @if (count($headCategories))
                            @foreach($headCategories as $row)
                                @if($row->categories->count() > 0)
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>
                                        @if(count($row->categories) > 0)
                                            @foreach($row->categories as $cats)
                                                {{$cats->name}}@if($loop->remaining != 0),&nbsp;@endif
                                            @endforeach
                                        @endif
                                    </td>
                                    @ability('','edit,delete')
                                    <td class="action">
                                        @permission(('edit'))
                                        <a href="{{ route('categories.edit_link', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                        @endpermission
                                        @permission(('delete'))
                                        <a href="{{ route('categories.show_link', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="Delete"></i> </a>
                                        @endpermission
                                    </td>
                                    @endability
                                </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop