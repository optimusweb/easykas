@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <h2>@lang('users.edit_user') </h2>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                            <a href="{{route('users.index_app')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" enctype="multipart/form-data" action="{{ route('users.update_app_user', ['id' => $user->id]) }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="well" style="max-height: 600px;overflow: auto;">

                                        <div class="form-group {{ $errors->has('companyName') ? ' has-error' : '' }}">
                                            <label for="companyName">@lang('users.companyName') <span class="required">*</span></label>
                                            <input type="text" value="{{ old('companyName') ?: $user->companyName }}" id="companyName" name="companyName" class="form-control">
                                            @if ($errors->has('companyName'))
                                                <span class="help-block">{{ $errors->first('companyName') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-sm-8 {{ $errors->has('street') ? ' has-error' : '' }}">
                                                <label for="street">@lang('users.street')</label>
                                                <input type="text" value="{{ old('street') ?: $user->street }}" id="street" name="street" class="form-control">
                                                @if ($errors->has('street'))
                                                    <span class="help-block">{{ $errors->first('street') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-2 {{ $errors->has('houseNumber') ? ' has-error' : '' }}">
                                                <label for="houseNumber">@lang('users.houseNumber')</label>
                                                <input type="text" value="{{ old('houseNumber') ?: $user->houseNumber }}" id="houseNumber" name="houseNumber" class="form-control">
                                                @if ($errors->has('houseNumber'))
                                                    <span class="help-block">{{ $errors->first('houseNumber') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-2 {{ $errors->has('suffix') ? ' has-error' : '' }}">
                                                <label for="suffix">@lang('users.suffix')</label>
                                                <input type="text" value="{{ old('suffix') ?: $user->suffix }}" id="suffix" name="suffix" class="form-control">
                                                @if ($errors->has('suffix'))
                                                    <span class="help-block">{{ $errors->first('suffix') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('zip') ? ' has-error' : '' }}">
                                            <label for="zip">@lang('users.zip')</label>
                                            <input type="text" value="{{ old('zip') ?: $user->zip }}" id="zip" name="zip" class="form-control">
                                            @if ($errors->has('zip'))
                                                <span class="help-block">{{ $errors->first('zip') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                            <label for="city">@lang('users.city')</label>
                                            <input type="text" value="{{ old('city') ?: $user->city }}" id="city" name="city" class="form-control">
                                            @if ($errors->has('city'))
                                                <span class="help-block">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                            <label for="logo">@lang('users.logo')</label>
                                            <input type="file" name="logo" class="form-control">
                                            @if ($errors->has('logo'))
                                                <span class="help-block">{{ $errors->first('logo') }}</span>
                                            @endif
                                        </div>

                                        @if(!empty($user->logo))
                                            <img src="{{ asset('/uploads/logos/'.$user->logo) }}" height="100px">
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="well" style="min-height: 300px;overflow: auto;">
                                        <div class="form-row">
                                            <div class="form-group col-md-6 {{ $errors->has('role_id') ? ' has-error' : '' }}">
                                                <label for="role_id">@lang('users.roles') <span class="required">*</span></label>
                                                <input type="text" class="form-control" value="{{$roles->where('name', 'app_user')->first()->display_name}}" disabled>
                                                <input type="hidden" name="role_id" value="{{$roles->where('name', 'app_user')->first()->id}}">

                                                @if ($errors->has('role_id'))
                                                    <span class="help-block">{{ $errors->first('role_id') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-6 {{ $errors->has('head_category_id') ? ' has-error' : '' }}">
                                                <label for="role_id">@lang('categories.category') <span class="required">*</span></label>
                                                <select class="form-control" id="head_category_id" name="head_category_id">
                                                    @if(count($headCategories) > 0)
                                                        <option value="">@lang('general.app.makechoice')</option>
                                                        @foreach($headCategories as $cats)
                                                            <option value="{{$cats->id}}" @if (($cats->id == old('head_category_id')) || (count($user->headCategory) > 0 && $user->headCategory->contains($cats->id))) {{ 'selected="selected"' }} @endif>{{$cats->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('head_category_id'))
                                                    <span class="help-block">{{ $errors->first('head_category_id') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        @foreach($settingTypes as $settingType)
{{--                                            {{dd($user->userSettingsData->contains('setting_type_id', $settingType->id))}}--}}
                                            <div class="form-group">
                                                <label for="{{$settingType->code}}">{{$settingType->name}}</label>
                                                <input type="text" value="@if(count($user->userSettingsData) > 0 && $user->userSettingsData->contains('setting_type_id', $settingType->id)){{$user->userSettingsData->where('setting_type_id', '=', $settingType->id)->first()->value}}@else{{old($settingType->code) ?: $settingType->settings->value }} @endif" id="{{$settingType->code}}" name="{{$settingType->code}}" class="form-control">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row col-md-12">
                                    <div class="well" style="max-height: 630px;overflow: auto;">
                                        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
                                            <label for="gender">@lang('users.gender')</label>
                                            <select class="form-control" name="gender">
                                                <option value="">@lang('general.app.makechoice')</option>
                                                <option value="M" @if ($user->gender == old('gender') || $user->gender == 'M') {{ 'selected="selected"' }} @endif>@lang('users.male')</option>
                                                <option value="F" @if ($user->gender == old('gender') || $user->gender == 'F') {{ 'selected="selected"' }} @endif>@lang('users.female')</option>
                                            </select>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-5 {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                                <label for="firstName">@lang('users.firstName') <span class="required">*</span></label>
                                                <input type="text" value="{{ old('firstName') ?: $user->firstName }}" id="firstName" name="firstName" class="form-control">
                                                @if ($errors->has('firstName'))
                                                    <span class="help-block">{{ $errors->first('firstName') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-2 {{ $errors->has('middleName') ? ' has-error' : '' }}">
                                                <label for="middleName">@lang('users.middleName')</label>
                                                <input type="text" value="{{ old('middleName') ?: $user->middleName }}" id="middleName" name="middleName" class="form-control">
                                                @if ($errors->has('middleName'))
                                                    <span class="help-block">{{ $errors->first('middleName') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-5 {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                                <label for="lastName">@lang('users.lastName') <span class="required">*</span></label>
                                                <input type="text" value="{{ old('lastName') ?: $user->lastName }}" id="lastName" name="lastName" class="form-control">
                                                @if ($errors->has('lastName'))
                                                    <span class="help-block">{{ $errors->first('lastName') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                                            <label for="email">@lang('users.email') <span class="required">*</span></label>
                                            <input type="text" value="{{ old('email') ?: $user->email }}" id="email" name="email" class="form-control">
                                            @if ($errors->has('email'))
                                                <span class="help-block">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('ocrMail') ? ' has-error' : '' }} row">
                                            <label for="email">@lang('users.ocrMail') <span class="required">*</span></label>
                                            <input type="text" value="{{ old('ocrMail') ?: $user->ocrmail }}" id="ocrMail" name="ocrMail" class="form-control">
                                            @if ($errors->has('ocrMail'))
                                                <span class="help-block">{{ $errors->first('ocrMail') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                                            <label for="password">@lang('users.password')</label>
                                            <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" class="form-control">
                                            @if ($errors->has('password'))
                                                <span class="help-block">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} row">
                                            <label for="confirm_password">@lang('users.confirm_password')</label>
                                            <input type="password" value="{{ Request::old('password_confirmation') ?: '' }}" id="password_confirmation" name="password_confirmation" class="form-control">
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="well" style="min-height: 50px;overflow: auto;">
                                        <div class="form-row" id="categories_tree">
                                            <label>Boekingstypen waarden worden hierin weergegeven. Selecteer eerst een Categorie links.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="hidden" id="user_id" value="{{ $user->id }}">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save_changes')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop