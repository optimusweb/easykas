@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>
                                @if($trashedItems == 1)
                                    @lang('users.deleted_users')
                                @else
                                    @lang('users.users_app')
                                @endif
                            </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            @if($trashedItems == 0)
                                <a href="@if($app_user == 1){{route('users.create_app_user')}}@else{{route('users.create')}}@endif" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                            @endif
                            @if($trashedItems == 0 && (isset($countItems) && $countItems > 0))
                                    <a href="{{route('users.deleted_app_users')}}" class="text-danger"><i class="fa fa-trash fa-lg" title="@lang('general.form.deleted_items')"></i> </a>
                                @elseif($trashedItems == 1)
                                    <a href="{{route('users.index_app')}}" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> @lang('general.form.active_items') </a>
                                @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('users.company')</th>
                                <th>@lang('users.name')</th>
                                <th>@lang('users.email')</th>
                                <th>@lang('users.ocrMail')</th>
                                <th>@lang('users.roles')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('users.action')</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('users.company')</th>
                                <th>@lang('users.name')</th>
                                <th>@lang('users.email')</th>
                                <th>@lang('users.ocrMail')</th>
                                <th>@lang('users.roles')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('users.action')</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if(count($users))
                                @foreach ($users as $row)
                                <tr>
                                    <td>{{$row->companyName}}</td>
                                    <td>{{$row->firstName}} @if($row->middleName) {{ $row->middleName }} @endif {{ $row->lastName }}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->ocrmail}}</td>
                                    <td>
                                    @foreach($row->roles as $r)
                                        <button title="{{$r->description}}" type="button" class="btn btn-success btn-xs">{{$r->display_name}}</button>
                                    @endforeach
                                    </td>
                                    @ability('','edit,delete')
                                        <td class="action">
                                        @permission(('edit'))
                                            @if($trashedItems == 0)
                                                <a href="@if($app_user == 1){{ route('users.edit_app_user', ['id' => $row->id]) }}@else{{ route('users.edit', ['id' => $row->id]) }}@endif" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                            @endif
                                        @endpermission
                                        @permission(('delete'))
                                            @if($trashedItems > 0)
                                                <a href="{{ route('users.activate_app_user', ['id' => $row->id]) }}" class="btn btn-success btn-xs"><i class="fa fa-thumbs-up" title="@lang('general.form.activate_app_user')"></i> </a>
                                                <a href="{{ route('users.activate', ['id' => $row->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-thumbs-up" title="@lang('general.form.activate')"></i> </a>
                                                <a href="{{ route('users.delete_app_user', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="@lang('general.form.activate')"></i> </a>
                                            @else
                                                <a href="{{ route('users.show_app', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="Delete"></i> </a>
                                            @endif
                                        @endpermission
                                        </td>
                                    @endability
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop