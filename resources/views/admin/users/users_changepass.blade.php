@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <h2>@lang('users.change_password') </h2>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                            <a href="{{route('users.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" enctype="multipart/form-data" action="{{ route('changepass') }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        {{--<div class="form-group{{ $errors->has('cur_password') ? ' has-error' : '' }}">--}}
                            {{--<label class="col-sm-2 col-form-label col-xs-12" for="cur_password">@lang('users.current_password') <span class="required">*</span></label>--}}
                            {{--<div class="col-md-3 col-sm-3 col-xs-12">--}}
                                {{--<input type="password" value="{{ Request::old('cur_password') ?: '' }}" id="cur_password" name="cur_password" class="form-control col-md-7 col-xs-12">--}}
                                {{--@if ($errors->has('cur_password'))--}}
                                    {{--<span class="help-block">{{ $errors->first('cur_password') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="password">@lang('users.new_password') <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="confirm_password">@lang('users.confirm_password') <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="password" value="{{ Request::old('password_confirmation') ?: '' }}" id="password_confirmation" name="password_confirmation" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save_changes')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop