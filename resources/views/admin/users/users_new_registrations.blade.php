@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('general.nav.new_registrations') </h2>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('users.company')</th>
                                <th>@lang('users.name')</th>
                                <th>@lang('users.email')</th>
                                <th>@lang('users.registered_at')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('users.action')</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('users.company')</th>
                                <th>@lang('users.name')</th>
                                <th>@lang('users.email')</th>
                                <th>@lang('users.registered_at')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('users.action')</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if(count($users))
                                @foreach ($users as $row)
                                <tr>
                                    <td>{{$row->companyName}}</td>
                                    <td>{{$row->firstName}} @if($row->middleName) {{ $row->middleName }} @endif {{ $row->lastName }}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{ Carbon\Carbon::parse($row->created_at)->format('d-m-Y') }}</td>
                                    @ability('','edit,delete')
                                    <td class="action">
                                        @permission(('edit'))
                                            <a href="{{ route('users.activateNewUser', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-thumbs-up" title="@lang('users.activate')"></i> </a>
                                        @endpermission
                                        @permission(('delete'))
                                            <a href="{{ route('users.destroy', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-thumbs-down" title="@lang('users.reject')"></i> </a>
                                        @endpermission
                                    </td>
                                    @endability
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop