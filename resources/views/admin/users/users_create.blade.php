@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('users.create_user') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('users.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" enctype="multipart/form-data" action="{{ route('users.store') }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well" style="max-height: 630px;overflow: auto;">
                                    <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label for="gender">@lang('users.gender')</label>
                                        <select class="form-control" name="gender">
                                            <option value="">@lang('general.app.makechoice')</option>
                                            <option value="M" @if (Request::old('gender') == "M") {{ 'selected="selected"' }} @endif>@lang('users.male')</option>
                                            <option value="F" @if (Request::old('gender') == "F") {{ 'selected="selected"' }} @endif>@lang('users.female')</option>
                                        </select>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-5 {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                            <label for="firstName">@lang('users.firstName') <span class="required">*</span></label>
                                            <input type="text" value="{{ Request::old('firstName') ?: '' }}" id="firstName" name="firstName" class="form-control">
                                            @if ($errors->has('firstName'))
                                                <span class="help-block">{{ $errors->first('firstName') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group col-md-2 {{ $errors->has('middleName') ? ' has-error' : '' }}">
                                            <label for="middleName">@lang('users.middleName')</label>
                                            <input type="text" value="{{ Request::old('middleName') ?: '' }}" id="middleName" name="middleName" class="form-control">
                                            @if ($errors->has('middleName'))
                                                <span class="help-block">{{ $errors->first('middleName') }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group col-md-5 {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                            <label for="lastName">@lang('users.lastName') <span class="required">*</span></label>
                                            <input type="text" value="{{ Request::old('lastName') ?: '' }}" id="lastName" name="lastName" class="form-control">
                                            @if ($errors->has('lastName'))
                                                <span class="help-block">{{ $errors->first('lastName') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                                        <label for="email">@lang('users.email') <span class="required">*</span></label>
                                        <input type="text" value="{{ Request::old('email') ?: '' }}" id="email" name="email" class="form-control">
                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                                        <label for="password">@lang('users.password') <span class="required">*</span></label>
                                        <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" class="form-control">
                                        @if ($errors->has('password'))
                                            <span class="help-block">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} row">
                                        <label for="confirm_password">@lang('users.confirm_password') <span class="required">*</span></label>
                                        <input type="password" value="{{ Request::old('password_confirmation') ?: '' }}" id="password_confirmation" name="password_confirmation" class="form-control">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
                                        <label for="role_id">@lang('users.roles') <span class="required">*</span></label>
                                        <select class="form-control" id="role_id" name="role_id">
                                            @if(count($roles))
                                                <option value="">@lang('general.app.makechoice')</option>
                                                @foreach($roles as $row)
                                                    <option value="{{$row->id}}" @if ($row->id == Request::old('role_id')) {{ 'selected="selected"' }} @endif>{{$row->display_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('role_id'))
                                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop