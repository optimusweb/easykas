@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <h2>@lang('information.information_icon')</h2>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 col-form-label col-xs-12" for="name">@lang('information.text')</label>
                        <div class="well col-xs-12" style="max-height: 630px;overflow: auto;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                @if(isset($information->text))
                                    {!! nl2br(e($information->text)) !!}
                                @else
                                    @lang('general.error.no_text_filled_in')
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="{{ route('information.edit', ['id' => 1]) }}"><button type="submit" class="btn btn-success">@lang('general.form.edit')</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop