@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            @if($id == 1)
                                <h2>@lang('information.edit_information_icon')</h2>
                            @else
                                <h2>@lang('information.edit_information_page')</h2>
                            @endif
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                            @if($id == 1)
                                <a href="{{route('information.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            @else
                                <a href="{{route('information.information_page')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" action="{{ route('information.update', ['id' => $id]) }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('information_text') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="name">@lang('information.text') <span class="required">*</span></label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <textarea name="information_text" id="information_text" class="form-control noresize @if($id == 1)text-icon @endif" rows="20">@if(isset($information->text)){{$information->text}}@endif</textarea>
                                @if ($errors->has('information_text'))
                                    <span class="help-block">{{ $errors->first('information_text') }}</span>
                                @endif
                            </div>
                        </div>

                        @if($id == 1)
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label><span class="text_icon_counter">250</span> @lang('general.app.characters_left')</label>
                            </div>
                        </div>
                        @endif

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop