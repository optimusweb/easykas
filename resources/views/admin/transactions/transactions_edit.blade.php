@extends('templates.admin.layout')

@section('content')
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <h2>@lang('transactions.create_transaction') </h2>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                                <a href="{{route('transactions.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form method="post" action="{{ route('transactions.update', ['id' => $transaction->id] ) }}" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label class="col-sm-2" for="user_id">@lang('users.user') <span class="required">*</span></label>
                                <div class="col-sm-4" id="transactions">
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <label>{{$user->firstName}} @if($user->middleName){{$user->middleName}}@endif {{$user->lastName}}</label>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('transaction_date') ? ' has-error' : '' }}">
                                <label class="col-sm-2">@lang('general.app.date')</label>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" name="transaction_date" placeholder="dd-mm-yyyy" value="{{\Carbon\Carbon::now()->format('d-m-Y')}}">
                                        <label>{{\Carbon\Carbon::now()->format('d-m-Y')}}</label>
                                    </div>

                                </div>
                            </div>
{{--{{dd($user)}}--}}
                            @if($id != 0 && count($user->headCategory) > 0)
                                @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                    @php $kassaldo_begin = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_begin')->first() @endphp
                                    <div class="form-group row" class="col-sm-12" id="bt_block_{{$kassaldo_begin->id}}">
                                        <label class="col-sm-2">{{ $kassaldo_begin->name }}</label>
                                        <div class="col-sm-1">
                                            <input type="text" value="{{ Request::old($kassaldo_begin->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $kassaldo_begin->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $kassaldo_begin->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $kassaldo_begin->id)->first()->id}}" class="form-control">
                                        </div>
                                        <span class="help-block hidden" id="{{"bt_error_".$kassaldo_begin->id}}">@lang('general.form.must_be_number')</span>
                                    </div>
                                @endif

                                @if($user->count() > 0 && count($user->headCategory()->get()) > 0)
                                    @foreach($user->headCategory()->first()->categories as $category)
                                        @if(mb_strtolower($category->name) != "overige" && mb_strtolower($category->name) != "opnemen/storten")
                                            <div class="well" style="max-height: 600px;overflow: auto;">
                                                <div class="form-group row" class="col-sm-12">
                                                    <label class="col-sm-4">{{ $category->name }}</label>
                                                </div>

                                                @if($category->bookingTypes()->count() > 0)
                                                    <div class="form-row" id="cat_block_{{$category->id}}">
                                                        @foreach($category->bookingTypes()->get() as $bookingType)
                                                            @if($bookingType->code != 'bonnen')
                                                                {{--{{dd($bookingType->name)}}--}}
                                                                <div class="form-group">
                                                                    <label for="{{$transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->id}}" class="@if($bookingType->code == 'salaris_personeel') col-sm-2 control-label text-right @else control-label col-sm-2 @endif">{{$bookingType->name}}</label>
                                                                    <div class="col-sm-1">
                                                                        <input type="text" value="{{ Request::old($bookingType->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->id}}" class="form-control">
                                                                    </div>
                                                                    <span class="help-block hidden" id="{{"bt_error_".$bookingType->id}}">@lang('general.form.must_be_number')</span>
                                                                </div>

                                                                @if($category->use_receipts)
                                                                    @php
                                                                        if(count($user_settings) > 0){
                                                                            $max_receipts = $user_settings->userSettingsData->where("setting_type_id", $user_settings->userSettings->where("code", "max_receipts")->first()->id)->first()->value;
                                                                        }else{
                                                                            $max_receipts = $settingTypes->where("code", "max_receipts")->first()->settings()->get()->first()->value;
                                                                        }
                                                                    @endphp
                                                                    <div class="form-group">
                                                                        <label for="receipt" class="col-sm-2 control-label text-right"></label>
                                                                        <div class="col-sm-4">
                                                                            @lang('transactions.file')
                                                                        </div>
                                                                        <div class="col-sm-1">
                                                                            @lang('transactions.value')
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            @lang('transactions.description')
                                                                        </div>
                                                                    </div>
                                                                    @for($t=0;$t<$max_receipts;$t++)
                                                                        @php $c = $t+1 @endphp
                                                                        <div class="form-group">
                                                                            <label for="receipt" class="col-sm-2 control-label text-right">@lang('transactions.receipt') {{$c}}</label>
                                                                            <div class="col-sm-4">
                                                                                <input type="file" value="{{ Request::old('receipt[]')}}" id="receipt[]" name="receipt[]" class="form-control">
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <input type="text" value="{{ Request::old('receipt_val[]')}}" id="receipt_val[]" name="receipt_val[]" class="form-control">
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <input type="text" value="{{ Request::old('receipt_desc[]')}}" id="receipt_desc[]" name="receipt_desc[]" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                            @if($loop->last)
                                                                <div class="form-group row">
                                                                    <label class="col-sm-1 col-sm-offset-3 border-top font-weight-bold" id="show_total_cat_{{$category->id}}"></label>
                                                                    <input type="hidden" name="cat_{{$category->id}}" id="cat_{{$category->id}}" value="">
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                    @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                        @php $kasmutatie = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasmutatie')->first() @endphp
                                        <div class="form-group row" class="col-sm-12" id="bt_block_{{$kasmutatie->id}}">
                                            <label class="col-sm-2">{{ $kasmutatie->name }}</label>
                                            <div class="col-sm-1 kasmutatie">
                                                <input type="text" value="{{ Request::old($kasmutatie->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $kasmutatie->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $kasmutatie->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $kasmutatie->id)->first()->id}}" class="form-control">
                                            </div>
                                            <span class="help-block hidden" id="{{"bt_error_".$kasmutatie->id}}">@lang('general.form.must_be_number')</span>
                                        </div>
                                        @php $telling_kasgeld = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'telling_kasgeld')->first() @endphp
                                        <div class="form-group row" class="col-sm-12" id="bt_block_{{$telling_kasgeld->id}}">
                                            <label class="col-sm-2">{{ $telling_kasgeld->name }}</label>
                                            <div class="col-sm-1">
                                                <input type="text" value="{{ Request::old($telling_kasgeld->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $telling_kasgeld->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $telling_kasgeld->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $telling_kasgeld->id)->first()->id}}" class="form-control">
                                            </div>
                                            <span class="help-block hidden" id="{{"bt_error_".$telling_kasgeld->id}}">@lang('general.form.must_be_number')</span>
                                        </div>
                                        @php $kasverschil = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasverschil')->first() @endphp
                                        <div class="form-group row" class="col-sm-12" id="bt_block_{{$kasverschil->id}}">
                                            <label class="col-sm-2">{{ $kasverschil->name }}</label>
                                            <div class="col-sm-1 kasverschil">
                                                <input type="text" value="{{ Request::old($kasverschil->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $kasverschil->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $kasverschil->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $kasverschil->id)->first()->id}}" class="form-control">
                                            </div>
                                            <span class="help-block hidden" id="{{"bt_error_".$kasverschil->id}}">@lang('general.form.must_be_number')</span>
                                        </div>
                                    @endif
                                    @foreach($user->headCategory()->first()->categories as $category)
                                        @if(mb_strtolower($category->name) == "opnemen/storten")
                                            <div class="well" style="max-height: 600px;overflow: auto;">
                                                <div class="form-group row" class="col-sm-12">
                                                    <label class="col-sm-4">{{ $category->name }}</label>
                                                </div>

                                                @if($category->bookingTypes()->count() > 0)
                                                    <div class="form-row" id="cat_block_{{$category->id}}">
                                                        @foreach($category->bookingTypes()->get() as $bookingType)
                                                            {{--{{dd($bookingType->name)}}--}}
                                                            <div class="form-group">
                                                                <label for="{{"bt_".$bookingType->id}}" class="@if($bookingType->code == 'salaris_personeel') col-sm-2 control-label text-right @else control-label col-sm-2 @endif">{{$bookingType->name}}</label>
                                                                <div class="col-sm-1">
                                                                    <input type="text" value="{{ Request::old($bookingType->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $bookingType->id)->first()->id}}" class="form-control">
                                                                </div>
                                                            </div>

                                                            @if($loop->last)
                                                                <div class="form-group row">
                                                                    <label class="col-sm-1 col-sm-offset-3 border-top font-weight-bold" id="show_total_cat_{{$category->id}}"></label>
                                                                    <input type="hidden" name="cat_{{$category->id}}" id="cat_{{$category->id}}" value="">
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                    @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                        @php $kassaldo_eind = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_eind')->first() @endphp
                                        <div class="form-group row" class="col-sm-12" id="bt_block_{{$kassaldo_eind->id}}">
                                            <label class="col-sm-2">{{ $kassaldo_eind->name }}</label>
                                            <div class="col-sm-1">
                                                <input type="text" value="{{ Request::old($kassaldo_eind->code) ?: $transaction->transActionValues->where('booking_type_id', '=', $kassaldo_eind->id)->first()->value }}" id="{{$transaction->transActionValues->where('booking_type_id', '=', $kassaldo_eind->id)->first()->id}}" name="{{$transaction->transActionValues->where('booking_type_id', '=', $kassaldo_eind->id)->first()->id}}" class="form-control">
                                            </div>
                                            <span class="help-block hidden" id="{{"bt_error_".$kassaldo_eind->id}}">@lang('general.form.must_be_number')</span>
                                        </div>
                                    @endif
                                @else
                                    <div class="form-group" class="col-sm-12">
                                        <label class="col-sm-12 col-md-offset-2 text-danger">Er is geen categorie gekoppeld aan de gebruiker. Stel deze in a.u.b.!</label>
                                    </div>
                                @endif

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-12 col-md-offset-2">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        <input name="_method" type="hidden" value="PUT">
                                        <button type="submit" class="btn btn-success">@lang('general.form.save_changes')</button>
                                    </div>
                                </div>
                            @elseif($id != 0)
                                <div class="form-group">
                                    <label class="col-sm-12 col-sm-offset-2 text-danger">@lang('transactions.no_categories_linked_to_user')</label>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop