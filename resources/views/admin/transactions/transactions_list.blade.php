@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>
                                @if($trashedItems == 1)
                                    @lang('transactions.deleted_transactions')
                                @else
                                    @lang('transactions.transactions')
                                @endif
                            </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            @if($trashedItems == 0)
                                <a href="{{route('transactions.create', ['id' => 0])}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                            @endif
                            @if($trashedItems == 0 && (isset($countItems) && $countItems > 0))
                                <a href="{{route('transactions.deleted_headcats')}}" class="text-danger"><i class="fa fa-trash fa-lg" title="@lang('general.form.deleted_items')"></i> </a>
                            @elseif($trashedItems == 1)
                                <a href="{{route('transactions.index')}}" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> @lang('general.form.active_items') </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('transactions.transaction')</th>
                                <th>@lang('users.user')</th>
                                <th>@lang('general.app.date')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('transactions.action')</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('transactions.transaction')</th>
                                <th>@lang('users.user')</th>
                                <th>@lang('general.app.date')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('transactions.action')</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($transactions))
                                @foreach($transactions as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{$row->users->firstName}} @if($row->users->middleName) {{ $row->users->middleName }} @endif {{ $row->users->lastName }}</td>
                                    <td>{{Carbon\Carbon::parse($row->transaction_date)->format('d-m-Y')}}</td>
                                    @ability('','edit,delete')
                                    <td>
                                        @permission(('edit'))
                                            @if($trashedItems == 0)
                                                <a href="{{ route('transactions.show', ['id' => $row->id]) }}" class="btn btn-success btn-xs"><i class="fa fa-search" title="Edit"></i> </a>
                                                <a href="{{ route('transactions.pdf', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-file-pdf-o" title="PDF"></i> </a>
                                                <a href="{{ route('transactions.edit', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                            @endif
                                        @endpermission
                                    </td>
                                    @endability
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop