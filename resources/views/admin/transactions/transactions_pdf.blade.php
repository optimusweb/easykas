<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        .body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12px;
        }

        .bold {
            font-weight: bold;
        }

        .border-top {
            border-top: 1px solid #e5e5e5;
        }

        .logo {
            text-align: center;
            /*background-image:url(''); background-repeat:no-repeat; height:100px; top: 10px; position:absolute;*/
        }
    </style>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- page content -->
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="logo"><img src="{{public_path().'/admin/images/logo.png'}}" height="40px"></div>
                                <br/>
                                <table width="100%" border="0" cellpadding="3px" cellpadding="0">
                                    <tr>
                                        <td class="bold" width="20%">@lang('users.user')</td>
                                        <td width="30%">{{$transactions->users->firstName}} @if($transactions->users->middleName) {{ $transactions->users->middleName }} @endif {{ $transactions->users->lastName }}</td>
                                        <td width="10%"></td>
                                        <td width="10%"></td>
                                        <td width="30%"></td>
                                    </tr>
                                    <tr>
                                        <td class="bold">@lang('general.app.date')</td>
                                        <td colspan="4">{{ Carbon\Carbon::parse($transactions->transaction_date)->format('d-m-Y') }}</td>
                                    </tr>

                                    <tr>
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    @if(count($user) > 0)
                                        @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                            @php $kassaldo_begin = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_begin')->first() @endphp
                                            <tr>
                                                <td class="bold">{{ $kassaldo_begin->name }}</td>
                                                <td colspan="2"></td>
                                                <td class="bold">{{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $kassaldo_begin->id)->first()->value)}}</td>
                                            </tr>
                                        @endif
    {{--                                    {{dd($kassaldo_begin->id)}}--}}
                                        <tr>
                                            <td colspan="5">&nbsp;</td>
                                        </tr>
                                        @foreach($user->headCategory()->first()->categories as $category)
                                            @if(mb_strtolower($category->name) != "overige" && mb_strtolower($category->name) != "opnemen/storten")
                                                <tr>
                                                    <td class="bold" colspan="5">{{ $category->name }}</td>
                                                </tr>

                                                @if($category->bookingTypes()->count() > 0)
                                                    @foreach($category->bookingTypes()->get() as $bookingType)
                                                        @if($bookingType->code != 'bonnen')
                                                            <tr>
                                                                <td></td>
                                                                <td class="bold">{{$bookingType->name}}</td>
                                                                <td colspan="3">@if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $bookingType->id)->first()->value)}}@else - @endif</td>
                                                            </tr>
                                                        @endif

                                                        @if($category->use_receipts && $transactions->receipts)
                                                            @foreach($transactions->receipts as $receipt)
                                                                <tr>
                                                                    <td></td>
                                                                    <td class="bold">@lang('transactions.receipt')</td>
                                                                    <td>{{$receipt->value}}</td>
                                                                    <td></td>
                                                                    <td>{{$receipt->description}}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif

                                                        @if($loop->last)
                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td class="border-top"></td>
                                                                <td class="bold border-top">@if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{General::checkEmpty($transactions->transactionValues->where('category_id', $category->id)->first()->value)}}@else - @endif</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <tr>
                                                    <td colspan="5">&nbsp;</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                            @php $kasmutatie = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasmutatie')->first() @endphp
                                                <tr>
                                                    <td class="bold">{{ $kasmutatie->name }}</td>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="bold">{{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $kasmutatie->id)->first()->value)}}</td>
                                                </tr>
                                            @php $telling_kasgeld = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'telling_kasgeld')->first() @endphp
                                                <tr>
                                                    <td class="bold">{{ $telling_kasgeld->name }}</td>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="bold">{{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $telling_kasgeld->id)->first()->value)}}</td>
                                                </tr>
                                            @php $kasverschil = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasverschil')->first() @endphp
                                                <tr>
                                                    <td class="bold">{{ $kasverschil->name }}</td>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="bold">{{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $kasverschil->id)->first()->value)}}</td>
                                                </tr>

                                                <tr>
                                                    <td colspan="5">&nbsp;</td>
                                                </tr>
                                        @endif
                                        @foreach($user->headCategory()->first()->categories as $category)
                                            @if(mb_strtolower($category->name) == "opnemen/storten")
                                                <tr>
                                                    <td class="bold" colspan="5">{{ $category->name }}</td>
                                                </tr>

                                                @if($category->bookingTypes()->count() > 0)
                                                    @foreach($category->bookingTypes()->get() as $bookingType)
                                                        <tr>
                                                            <td></td>
                                                            <td class="bold">{{$bookingType->name}}</td>
                                                            <td colspan="3">@if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $bookingType->id)->first()->value)}}@else - @endif</td>
                                                        </tr>

                                                        @if($loop->last)
                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td class="border-top"></td>
                                                                <td class="bold border-top">@if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{General::checkEmpty($transactions->transactionValues->where('category_id', $category->id)->first()->value)}}@else - @endif</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                        @if($user->headCategory->first()->categories()->where('name', 'Overige')->first() != null)
                                            <tr>
                                                <td colspan="5">&nbsp;</td>
                                            </tr>
                                            @php $kassaldo_eind = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_eind')->first() @endphp
                                                <tr>
                                                    <td class="bold">{{ $kassaldo_eind->name }}</td>
                                                    <td colspan="2"></td>
                                                    <td colspan="2" class="bold">{{General::checkEmpty($transactions->transactionValues->where('booking_type_id', $kassaldo_eind->id)->first()->value)}}</td>
                                                </tr>
                                        @endif
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /page content -->
    </div>
</div>
</body>
</html>
{{--{{dd()}}--}}