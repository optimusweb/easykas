@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">
{{--{{dd($userId)}}--}}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>
                                @if($trashedItems == 1)
                                    @lang('transactions.deleted_transactions')
                                @else
                                    @lang('transactions.overview_quarters')
                                @endif
                            </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            @if($trashedItems == 0)
                                <a href="{{route('transactions.create', ['id' => 0])}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                            @endif
                            @if($trashedItems == 0 && (isset($countItems) && $countItems > 0))
                                <a href="{{route('transactions.deleted_headcats')}}" class="text-danger"><i class="fa fa-trash fa-lg" title="@lang('general.form.deleted_items')"></i> </a>
                            @elseif($trashedItems == 1)
                                <a href="{{route('transactions.index')}}" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> @lang('general.form.active_items') </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <form method="post" action="{{ route('transactions.get_mutation_quarters') }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        <div class="well" style="max-height: 300px;overflow: auto;">
                            <div class="form-row">
                                <div class="form-group col-md-5 {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                    <label for="role_id" class="col-sm-12">@lang('mutations.select_customer') <span class="required">*</span></label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="user_id" name="user_id">
                                            @if(count($users) > 0)
                                                <option value="">@lang('general.app.makechoice')</option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}" @if (isset($userId) && ($user->id == $userId)) {{ 'selected="selected"' }} @endif>{{$user->firstName}} @if($user->middleName) {{ $user->middleName }} @endif {{ $user->lastName }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('user_id'))
                                            <span class="help-block">{{ $errors->first('user_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="col-sm-12">Date Range</label>
                                    <div class="col-sm-12">
                                        <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span></span>
                                            <input type="hidden" name="date_range" id="date_range" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="col-sm-12">&nbsp;</label>
                                    <div class="col-sm-12">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        <button type="submit" class="btn btn-success">@lang('general.form.get_data')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    @if (count($transactions) > 0)
                    <table id="datatable-no-search" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('general.app.date')</th>
                                <th>@lang('transactions.description')</th>
                                <th>@lang('transactions.ontvangsten_incl_btw')</th>
                                <th>@lang('transactions.uitgaven_incl_btw')</th>
                                <th>@lang('transactions.saldo')</th>
                            </tr>
                            <tr>
                                <th colspan="4">@lang('mutations.beginsaldo')</th>
                                <th>{{$kassaldo_begin}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $saldo = $kassaldo_begin @endphp
                            @foreach($transactions as $row)
                                @foreach($row->transactionValues as $val)
                                    @if (!isset($val->categories) && !empty($val->value))
                                        @php
                                            $uitgaven = '';
                                            $ontvangsten = '';
                                            $text_color = '';
                                            $btw = 21;
                                            //if ($val->category_id != null && ($omzet_id == $val->category_id || $betaalmethode_id == $val->category_id)) {
                                            if ((count($val->categories) > 0 && $val->categories->is_negative) || $val->bookingTypes->is_negative){
                                                // the value is negative, decrease from the saldo with the value
                                                $uitgaven = $val->value;
                                                $saldo -= $uitgaven;
                                                $text_color = "class=text-danger";
                                            }elseif($val->value == 0 || $val->value > 0){
                                            //if($val->category_id != null && ($kosten_id == $val->category_id || $opnemen_storten_id == $val->category_id)){
                                                // the value is positive, increase the saldo with the value
                                                $ontvangsten = $val->value;
                                                $saldo += $ontvangsten;
                                                $text_color = "class=green";
                                            }
                                        @endphp
                                        {{--{{dd()}}--}}
                                        <tr>
                                            <td>{{Carbon\Carbon::parse($row->transaction_date)->format('d-m-Y')}}</td>
                                            <td>@if(isset($val->bookingTypes)){{$val->bookingTypes->description}}@endif</td>
                                            <td>{{$ontvangsten}}</td>
                                            <td>{{$uitgaven}}</td>
                                            <td {{$text_color}}>{{$saldo}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                @if(isset($row->receipts))
                                    @foreach($row->receipts as $receipt)
                                        @if (!empty($receipt->value))
                                            @php
                                                $uitgaven = '';
                                                $ontvangsten = '';
                                                $text_color = '';

                                                if ($receipt->bookingTypes->is_negative){
                                                    // the value is negative, decrease from the saldo with the value
                                                    $uitgaven = $receipt->value;
                                                    $saldo -= $uitgaven;
                                                    $text_color = "class=text-danger";
                                                }elseif($receipt->value >= 0){
                                                //if($val->category_id != null && ($kosten_id == $val->category_id || $opnemen_storten_id == $val->category_id)){
                                                    // the value is positive, increase the saldo with the value
                                                    $ontvangsten = $receipt->value;
                                                    $saldo += $ontvangsten;
                                                    $text_color = "class=green";
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{Carbon\Carbon::parse($row->transaction_date)->format('d-m-Y')}}</td>
                                                <td>{{$receipt->description}}</td>
                                                <td>{{$ontvangsten}}</td>
                                                <td>{{$uitgaven}}</td>
                                                <td {{$text_color}}>{{$saldo}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                        @php
                        if($saldo >= 0){
                            $endclass = "class=green";
                        }else{
                            $endclass = "class=text-danger";
                        }
                        @endphp
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-right">@lang('transactions.saldo') {{$endDate}}</th>
                                <th {{$endclass}}>{{$saldo}}</th>
                            </tr>
                        </tfoot>
                    </table>
                    @elseif(count($transactions) == 0 && $userId != 0)
                        <div class="form-group">
                            <label class="col-sm-12 text-danger">@lang('transactions.no_results_found')</label>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop