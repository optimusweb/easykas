@extends('templates.admin.layout')

@section('content')
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <h2>@lang('transactions.create_transaction') </h2>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                                <a href="{{route('transactions.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group row">
                            <label class="col-sm-2" for="user_id">@lang('users.user')</label>
                            <div class="col-sm-4">
                                {{$transactions->users->firstName}} @if($transactions->users->middleName) {{ $transactions->users->middleName }} @endif {{ $transactions->users->lastName }}
{{--                            {{dd($transactions->users->firstName)}}--}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2">@lang('general.app.date')</label>
                            <div class="col-md-3 col-sm-3 col-xs-12 font-weight-bold">
                                {{ Carbon\Carbon::parse($transactions->date)->format('d-m-Y') }}
                            </div>
                        </div>
                        @if(count($user) > 0)
                            @php $kassaldo_begin = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_begin')->first() @endphp
                            <div class="form-group row" class="col-sm-12">
                                <label class="col-sm-4">{{ $kassaldo_begin->name }}</label>
                                <div class="col-sm-1 font-weight-bold">
                                    {{$transactions->transactionValues->where('booking_type_id', $kassaldo_begin->id)->first()->value}}
                                </div>
                            </div>
                            {{--{{dd($kassaldo_begin->id)}}--}}
                            @foreach($user->headCategory()->first()->categories as $category)
                                @if(mb_strtolower($category->name) != "overige")
                                    <div class="well" style="max-height: 600px;overflow: auto;">
                                        <div class="form-group row" class="col-sm-12">
                                            <label class="col-sm-4">{{ $category->name }}</label>
                                        </div>

                                        @if($category->bookingTypes()->count() > 0)
                                            <div class="form-row" id="cat_block_{{$category->id}}">
                                                @foreach($category->bookingTypes()->get() as $bookingType)
{{--                                                    {{$bookingType->name}}--}}
                                                    <div class="form-group row">
                                                        <label for="{{"bt_".$bookingType->id}}" class="col-sm-2 col-sm-offset-1">{{$bookingType->name}}</label>
                                                        <div class="col-sm-1">
                                                            @if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{$transactions->transactionValues->where('booking_type_id', $bookingType->id)->first()->value}}@else - @endif
                                                        </div>
                                                    </div>

                                                    @if($category->use_receipts && $transactions->receipts)
                                                        @foreach($transactions->receipts as $receipt)
                                                        <div class="form-group row">
                                                            <div class="col-sm-1">
                                                                <img src="{{asset('/uploads/receipts/'.$transactions->id."/".$receipt->image)}}" width="50px">
                                                            </div>
                                                            <label for="receipt" class="col-sm-1">@lang('transactions.receipt')</label>
                                                            <div class="col-sm-1 col-sm-offset-1">
                                                                {{$receipt->value}}
                                                            </div>
                                                            <div class="col-sm-4 col-sm-offset-1">
                                                                {{$receipt->description}}
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    @endif

                                                    @if($loop->last)
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-sm-offset-3 border-top font-weight-bold" id="show_total_cat_{{$category->id}}"></label>
                                                            <input type="hidden" name="cat_{{$category->id}}" id="cat_{{$category->id}}" value="">
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-sm-offset-2"></label>
                                                    <div class="col-sm-1">
                                                        @if($transactions->transactionValues->contains('booking_type_id', $bookingType->id)){{$transactions->transactionValues->where('category_id', $category->id)->first()->value}}@else - @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                            @endforeach
                            @php $kasmutatie = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasmutatie')->first() @endphp
                            <div class="form-group row" class="col-sm-12">
                                <label class="col-sm-4">{{ $kasmutatie->name }}</label>
                                <div class="col-sm-1">
                                    {{$transactions->transactionValues->where('booking_type_id', $kasmutatie->id)->first()->value}}
                                </div>
                            </div>
                            @php $telling_kasgeld = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'telling_kasgeld')->first() @endphp
                            <div class="form-group row" class="col-sm-12">
                                <label class="col-sm-4">{{ $telling_kasgeld->name }}</label>
                                <div class="col-sm-1">
                                    {{$transactions->transactionValues->where('booking_type_id', $telling_kasgeld->id)->first()->value}}
                                </div>
                            </div>
                            @php $kasverschil = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kasverschil')->first() @endphp
                            <div class="form-group row" class="col-sm-12">
                                <label class="col-sm-4">{{ $kasverschil->name }}</label>
                                <div class="col-sm-1">
                                    {{$transactions->transactionValues->where('booking_type_id', $kasverschil->id)->first()->value}}
                                </div>
                            </div>
                            @php $kassaldo_eind = $user->headCategory->first()->categories()->where('name', 'Overige')->first()->bookingTypes()->where('code', 'kassaldo_eind')->first() @endphp
                            <div class="form-group row" class="col-sm-12">
                                <label class="col-sm-4">{{ $kassaldo_eind->name }}</label>
                                <div class="col-sm-1">
                                    {{$transactions->transactionValues->where('booking_type_id', $kassaldo_eind->id)->first()->value}}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop