@extends('templates.admin.layout')

@section('content')
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <h2>@lang('transactions.create_transaction') </h2>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                                <a href="{{route('transactions.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form method="post" action="{{ route('transactions.store') }}" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label class="col-sm-2" for="user_id">@lang('users.user') <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="user_id" name="user_id">
                                        @if(count($users))
                                            <option value="">@lang('general.app.makechoice')</option>
                                            @foreach($users as $row)
                                                <option value="{{$row->id}}" @if ($row->id == Request::old('user_id') || $row->id == $id) {{ 'selected="selected"' }} @endif>{{$row->firstName}} @if($row->middleName) {{ $row->middleName }} @endif {{ $row->lastName }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">{{ $errors->first('user_id') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2">@lang('general.app.date')</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    {{ $date }}
                                </div>
                            </div>
                            @if($id != 0)
                                @if($user->count() > 0)
                                    @foreach($user->headCategory()->first()->categories as $category)
                                        <div class="form-group" class="col-sm-12">
                                            <label>{{ $category->name }}</label>
                                        </div>

                                        @if($category->bookingTypes()->count() > 0)
                                            <div class="form-row" id="cat_block_{{$category->id}}">
                                            @foreach($category->bookingTypes()->get() as $bookingType)
                                                {{--{{dd($bookingType->name)}}--}}
                                                <div class="form-group">
                                                    <label for="{{"bt_".$bookingType->id}}" class="col-sm-2">{{$bookingType->name}}</label>
                                                    <div class="col-sm-1">
                                                        <input type="text" value="{{ Request::old($bookingType->code) ?: '' }}" id="{{"bt_".$bookingType->id}}" name="{{"bt_".$bookingType->id}}" class="form-control">
                                                    </div>
                                                </div>

                                                @if($category->use_receipts)
                                                    @php
                                                        if($user_settings->count != null){
                                                            $max_receipts = $user_settings->userSettingsData->where("setting_type_id", $user_settings->userSettings->where("code", "max_receipts")->first()->id)->first()->value;
                                                        }else{
                                                            $max_receipts = $settingTypes->where("code", "max_receipts")->first()->settings()->get()->first()->value;
                                                        }
                                                    @endphp
                                                    @for($t=0;$t<$max_receipts;$t++)
                                                        @php $c = $t+1 @endphp
                                                        <div class="form-group">
                                                            <label for="receipt" class="col-sm-2">@lang('transactions.receipt') {{$c}}</label>
                                                            <div class="col-sm-4">
                                                                <input type="file" value="{{ Request::old('receipt[]')}}" id="receipt[]" name="receipt[]" class="form-control">
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <input type="text" value="{{ Request::old('receipt_val[]')}}" id="receipt_val[]" name="receipt_val[]" class="form-control">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" value="{{ Request::old('receipt_desc[]')}}" id="receipt_desc[]" name="receipt_desc[]" class="form-control">
                                                            </div>
                                                        </div>
                                                    @endfor
                                                @endif

                                                @if($loop->last)
                                                    <div class="form-group">
                                                        <label class="col-sm-2 col-sm-offset-3" id="show_total_cat_{{$category->id}}"></label>
                                                        <input type="hidden" name="cat_{{$category->id}}" id="cat_{{$category->id}}" value="">
                                                    </div>
                                                @endif
                                            @endforeach
                                            </div>
                                        @endif
                                    @endforeach
                                @endif

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-12 col-md-offset-2">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop