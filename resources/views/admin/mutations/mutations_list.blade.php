@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">
{{--{{dd($userId)}}--}}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>
                                @if($trashedItems == 1)
                                    @lang('transactions.deleted_transactions')
                                @else
                                    @lang('transactions.overview_mutations')
                                @endif
                            </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            @if($trashedItems == 0)
                                <a href="{{route('transactions.create', ['id' => 0])}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                            @endif
                            @if($trashedItems == 0 && (isset($countItems) && $countItems > 0))
                                <a href="{{route('transactions.deleted_headcats')}}" class="text-danger"><i class="fa fa-trash fa-lg" title="@lang('general.form.deleted_items')"></i> </a>
                            @elseif($trashedItems == 1)
                                <a href="{{route('transactions.index')}}" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> @lang('general.form.active_items') </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <form method="post" action="{{ route('transactions.get_mutations') }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        <div class="well" style="max-height: 300px;overflow: auto;">
                            <div class="form-row">
                                <div class="form-group col-md-5 {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                    <label for="role_id" class="col-sm-12">@lang('mutations.select_customer') <span class="required">*</span></label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="user_id" name="user_id">
                                            @if(count($users) > 0)
                                                <option value="">@lang('general.app.makechoice')</option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}" @if (isset($userId) && ($user->id == $userId)) {{ 'selected="selected"' }} @endif>{{$user->firstName}} @if($user->middleName) {{ $user->middleName }} @endif {{ $user->lastName }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('user_id'))
                                            <span class="help-block">{{ $errors->first('user_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="col-sm-12">Date Range</label>
                                    <div class="col-sm-12">
                                        <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span></span>
                                            <input type="hidden" name="date_range" id="date_range" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="col-sm-12">&nbsp;</label>
                                    <div class="col-sm-12">
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                        <button type="submit" class="btn btn-success">@lang('general.form.get_data')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    @if (count($transactions) > 0)
                    <table id="datatable-no-search" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('transactions.boekjaar')</th>
                                <th>@lang('transactions.periode')</th>
                                <th>@lang('general.app.date')</th>
                                <th>@lang('transactions.gb_rek')</th>
                                <th>@lang('transactions.description')</th>
                                <th>@lang('transactions.bedrag')</th>
                                <th>@lang('transactions.btw_code')</th>
                                <th class="hidden">@lang('transactions.btw_percentage')</th>
                                <th class="hidden">@lang('transactions.btw_bedrag')</th>
                                <th class="hidden">@lang('transactions.opmerkingen')</th>
                                <th class="hidden">@lang('transactions.project')</th>
                                <th class="hidden">@lang('transactions.wisselkoers')</th>
                                <th class="hidden">@lang('transactions.kostenplaats_code')</th>
                                <th class="hidden">@lang('transactions.kostenplaats_omschr')</th>
                                <th class="hidden">@lang('transactions.kostendrager_code')</th>
                                <th class="hidden">@lang('transactions.kostendrager_omschr')</th>
                                <th class="hidden">@lang('transactions.code')</th>
                                <th class="hidden">@lang('transactions.name')</th>
                                {{--<th class="hidden">@lang('transactions.valuta')</th>--}}
                                {{--<th>@lang('transactions.beginsaldo')</th>--}}
                                {{--<th class="hidden">@lang('transactions.onze_ref')</th>--}}
                                {{--<th class="hidden">@lang('transactions.count')</th>--}}
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('transactions.boekjaar')</th>
                                <th>@lang('transactions.periode')</th>
                                <th>@lang('general.app.date')</th>
                                <th>@lang('transactions.gb_rek')</th>
                                <th>@lang('transactions.description')</th>
                                <th>@lang('transactions.bedrag')</th>
                                <th>@lang('transactions.btw_code')</th>
                                <th class="hidden">@lang('transactions.btw_percentage')</th>
                                <th class="hidden">@lang('transactions.btw_bedrag')</th>
                                <th class="hidden">@lang('transactions.opmerkingen')</th>
                                <th class="hidden">@lang('transactions.project')</th>
                                <th class="hidden">@lang('transactions.wisselkoers')</th>
                                <th class="hidden">@lang('transactions.kostenplaats_code')</th>
                                <th class="hidden">@lang('transactions.kostenplaats_omschr')</th>
                                <th class="hidden">@lang('transactions.kostendrager_code')</th>
                                <th class="hidden">@lang('transactions.kostendrager_omschr')</th>
                                <th class="hidden">@lang('transactions.code')</th>
                                <th class="hidden">@lang('transactions.name')</th>
                                {{--<th class="hidden">@lang('transactions.valuta')</th>--}}
                                {{--<th>@lang('transactions.beginsaldo')</th>--}}
                                {{--<th class="hidden">@lang('transactions.onze_ref')</th>--}}
                                {{--<th class="hidden">@lang('transactions.count')</th>--}}
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($transactions as $row)
                                @foreach($row->transactionValues as $val)
                                    {{--@php dd($val->bookingTypes) @endphp--}}
                                    @if(!empty($val->bookingTypes->code) && $val->bookingTypes->code != 'telling_kasgeld' && $val->bookingTypes->code != 'kassaldo_begin' && $val->bookingTypes->code != 'kasmutatie' && $val->bookingTypes->code != 'kassaldo_eind')
                                        @php $btw = 21; @endphp
                                        <tr>
                                            <td>{{Carbon\Carbon::parse($row->transaction_date)->format('Y')}}</td>
                                            <td>{{Carbon\Carbon::parse($row->transaction_date)->format('m')}}</td>
                                            <td>{{Carbon\Carbon::parse($row->transaction_date)->format('d-m-Y')}}</td>
                                            <td>@if(!empty($val->bookingTypes->gb_rek)) {{$val->bookingTypes->gb_rek}} @endif</td>
                                            <td>@if(!empty($val->description)) {{$val->description}} @elseif(!empty($val->bookingTypes->description)) {{$val->bookingTypes->description}} @endif</td>
                                            <td>@if($val->bookingTypes->is_negative && !empty($val->value))-{{$val->value}}@else{{$val->value}}@endif</td>
                                            <td>@if(!empty($val->bookingTypes->btw_code)) {{$val->bookingTypes->btw_code}} @endif</td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            @if($id = $val->where('booking_type_id', '=', $kassaldo_eind_id)->first()->id)
                                                @php $kassaldo_eind = $val->find($id)->value @endphp
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                                @foreach($row->receipts as $receipt)
{{--                                    {{dd($receipt->bookingTypes)}}--}}
                                    <tr>
                                        <td>{{Carbon\Carbon::parse($row->transaction_date)->format('Y')}}</td>
                                        <td>{{Carbon\Carbon::parse($row->transaction_date)->format('m')}}</td>
                                        <td>{{Carbon\Carbon::parse($row->transaction_date)->format('d-m-Y')}}</td>
                                        <td>@if(!empty($receipt->bookingTypes->gb_rek)) {{$receipt->bookingTypes->gb_rek}} @endif</td>
                                        <td>@if(!empty($receipt->description)) {{$receipt->description}} @elseif(!empty($receipt->bookingTypes->description)) {{$receipt->bookingTypes->description}} @endif</td>
                                        <td>{{$receipt->value}}</td>
                                        <td>@if(!empty($receipt->bookingTypes->btw_code)) {{$receipt->bookingTypes->btw_code}} @endif</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        {{--@if($id = $receipt->where('booking_type_id', '=', $kassaldo_eind_id)->first()->id)--}}
                                            {{--@php $kassaldo_eind = $receipt->find($id)->value @endphp--}}
                                        {{--@endif--}}
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop