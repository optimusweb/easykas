@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('bookingTypes.bookingTypes') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('bookingTypes.link')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <form method="post" action="{{ route('bookingTypes.store_link') }}" data-parsley-validate class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label class="col-sm-12 col-form-label col-xs-12" for="name">@lang('categories.category') <span class="required">*</span></label>
                                <select name="category" class="form-control">
                                    <option value="">@lang('general.app.makechoice')</option>
                                    @foreach($categories as $item)
                                        @if(!$category_bookingTypes->contains($item->id))
                                            <option value="{{$item->id}}" @if(Request::old('category') == $item->id) {{ "selected='selected'" }} @endif>{{$item->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">{{ $errors->first('category') }}</span>
                                @endif
                            </div>

                            <div class="col-md-10 col-sm-10 col-xs-12 {{ $errors->has('bookingType') ? ' has-error' : '' }}">
                                <label class="col-sm-12 col-form-label col-xs-12" for="name">@lang('bookingTypes.bookingTypes') <span class="required">*</span></label>
                                <table id="datatable-checkbox" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>@lang('bookingTypes.bookingType')</th>
                                        <th>@lang('bookingTypes.description')</th>
                                        <th>@lang('bookingTypes.code')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($bookingTypes))
                                        @foreach($bookingTypes as $row)
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox{{ $row->id }}" type="checkbox" name="bookingType[]" value="{{ $row->id }}" @if ($row->set_default == 1) checked="" @endif >
                                                        <label for="checkbox{{ $row->id }}"></label>
                                                    </div>
                                                </td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->description}}</td>
                                                <td>{{$row->code}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{--<div class="well" style="min-height: 100px;overflow: auto;">--}}
                                    {{--@foreach($bookingTypes as $val)--}}
                                    {{--<div class="checkbox checkbox-primary">--}}
                                        {{--<input id="checkbox{{ $val->id }}" type="checkbox" name="bookingType[]" value="{{ $val->id }}" @if ($val->set_default == 1) checked="" @endif >--}}
                                        {{--<label for="checkbox{{ $val->id }}">{{ $val->name }}</label>--}}
                                    {{--</div>--}}
                                    {{--@endforeach--}}
                                {{--</div>--}}
                                @if ($errors->has('bookingType'))
                                    <span class="help-block">{{ $errors->first('bookingType') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop