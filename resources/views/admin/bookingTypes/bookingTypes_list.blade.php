@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>
                                @if($trashedItems == 1)
                                    @lang('bookingTypes.deleted_bookingTypes')
                                @else
                                    @lang('bookingTypes.bookingTypes')
                                @endif
                            </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            @if($trashedItems == 0)
                                <a href="{{route('bookingTypes.create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> @lang('general.app.create_new') </a>
                            @endif
                            @if($trashedItems == 0 && (isset($countItems) && $countItems > 0))
                                <a href="{{route('bookingTypes.deleted_bookingtypes')}}" class="text-danger"><i class="fa fa-trash fa-lg" title="@lang('general.form.deleted_items')"></i> </a>
                            @elseif($trashedItems == 1)
                                <a href="{{route('bookingTypes.index')}}" class="btn btn-success btn-xs"><i class="fa fa-arrow-left"></i> @lang('general.form.active_items') </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('bookingTypes.bookingType')</th>
                                <th>@lang('bookingTypes.description')</th>
                                <th>@lang('bookingTypes.gb_rek')</th>
                                <th>@lang('bookingTypes.default')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('bookingTypes.action')</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@lang('bookingTypes.bookingType')</th>
                                <th>@lang('bookingTypes.description')</th>
                                <th>@lang('bookingTypes.gb_rek')</th>
                                <th>@lang('bookingTypes.default')</th>
                                @ability('','edit,delete')
                                <th class="action">@lang('bookingTypes.action')</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($bookingTypes))
                                @foreach($bookingTypes as $row)
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->description}}</td>
                                    <td>{{$row->gb_rek}}</td>
                                    <td>@if($row->set_default == 1) <i class="fa fa-check-square-o fa-lg text-success"></i> @else <i class="fa fa-times fa-lg text-danger"></i> @endif</td>
                                    @ability('','edit,delete')
                                    <td class="action">
                                        @permission(('edit'))
                                            @if($trashedItems == 0)
                                                <a href="{{ route('bookingTypes.edit', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="@lang('general.form.edit')"></i> </a>
                                            @endif
                                        @endpermission
                                        @permission(('delete'))
                                            @if($trashedItems == 1)
                                                <a href="{{ route('bookingTypes.activate', ['id' => $row->id]) }}" class="btn btn-success btn-xs"><i class="fa fa-thumbs-up" title="@lang('general.form.activate')"></i> </a>
                                                <a href="{{ route('bookingTypes.delete', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="@lang('general.form.activate')"></i> </a>
                                            @else
                                                <a href="{{ route('bookingTypes.show', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="@lang('general.form.delete')"></i> </a>
                                            @endif
                                        @endpermission
                                    </td>
                                    @endability
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop