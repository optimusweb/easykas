@extends('templates.admin.layout')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <h2>@lang('bookingTypes.bookingTypes') </h2>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                            <a href="{{route('bookingTypes.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <form method="post" action="{{ route('bookingTypes.update', ['id' => $bookingType->id]) }}" data-parsley-validate class="form-horizontal form-label-left">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="name">@lang('bookingTypes.name') <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ old('name') ?: $bookingType->name }}" id="name" name="name" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="code">@lang('bookingTypes.code') <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ old('code') ?: $bookingType->code }}" id="code_show" class="form-control col-md-7 col-xs-12" disabled>
                                <input type="hidden" value="{{ old('code') ?: $bookingType->code }}" id="code" name="code" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('code'))
                                    <span class="help-block">{{ $errors->first('code') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="description">@lang('bookingTypes.description') <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ old('name') ?: $bookingType->description }}" id="description" name="description" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('description'))
                                    <span class="help-block">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gb_rek') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="gb_rek">@lang('bookingTypes.gb_rek') </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" value="{{ old('name') ?: $bookingType->gb_rek }}" id="gb_rek" name="gb_rek" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('gb_rek'))
                                    <span class="help-block">{{ $errors->first('gb_rek') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('btw_code') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="btw_code">@lang('bookingTypes.btw_code')</label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <select id="btw_code" name="btw_code" class="form-control">
                                    <option value="" @if ($bookingType->btw_code == '' || old('btw_code') == '') {{ 'selected="selected"' }} @endif>@lang('general.app.makechoice')</option>
                                    <option value="0" @if (($bookingType->btw_code == 0 && $bookingType->btw_code != '') || (old('btw_code') == 0 && old('btw_code') != '')) {{ 'selected="selected"' }} @endif>0</option>
                                    <option value="3" @if ($bookingType->btw_code == 3 || old('btw_code') == 3) {{ 'selected="selected"' }} @endif>3</option>
                                    <option value="4" @if ($bookingType->btw_code == 4 || old('btw_code') == 4) {{ 'selected="selected"' }} @endif>4</option>
                                </select>
                                @if ($errors->has('btw_code'))
                                    <span class="help-block">{{ $errors->first('btw_code') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="category_id">@lang('categories.category') <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <select id="category_id" name="category_id" class="form-control">
                                    @if(count($categories))
                                        <option value="">@lang('general.app.makechoice')</option>
                                        @foreach($categories as $row)
                                            <option value="{{$row->id}}" @if ($row->id == old('category_id') || $bookingType->categories->contains($row->id)) {{ 'selected="selected"' }} @endif>{{$row->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('category_id'))
                                    <span class="help-block">{{ $errors->first('category_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('default_id') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="default_id">@lang('general.app.set_as_default') </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox" type="checkbox" name="default_id" value="1" @if (old('default_id') == 1 || $bookingType->set_default == 1)) checked="" @endif >
                                    <label for="checkbox"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('is_negative') ? ' has-error' : '' }}">
                            <label class="col-sm-2 col-form-label col-xs-12" for="is_negative">@lang('categories.is_negative') </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="is_negative" type="checkbox" name="is_negative" value="1" @if (old('is_negative') == 1 || $bookingType->is_negative == 1))) checked="" @endif >
                                    <label for="is_negative"></label>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <input name="_method" type="hidden" value="PUT">
                                <button type="submit" class="btn btn-success">@lang('general.form.save_changes')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop