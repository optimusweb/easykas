@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <h2>@lang('categories.categories') </h2>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            <a href="{{route('bookingTypes.link')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <form method="post" action="{{ route('bookingTypes.update_link', ['id' => $category->id]) }}" data-parsley-validate class="form-horizontal form-label-left">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <p class="h4">{{ $category->name }}</p>
                            </div>

                            <div class="col-md-10 col-sm-10 col-xs-12 {{ $errors->has('category') ? ' has-error' : '' }}">
                                <div class="well" style="min-height: 100px;overflow: auto;">
                                    @foreach($bookingTypes as $val)
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox{{ $val->id }}" type="checkbox" name="bookingType[]" value="{{ $val->id }}" @if($category_bookingTypes->contains($val->id)) checked="" @endif>
                                            <label for="checkbox{{ $val->id }}">{{ $val->name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                                @if ($errors->has('category'))
                                    <span class="help-block">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-2">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop