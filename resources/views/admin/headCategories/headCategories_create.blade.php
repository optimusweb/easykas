@extends('templates.admin.layout')

@section('content')
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <h2>@lang('headCategories.create_head_category') </h2>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                                <a href="{{route('headCategories.index')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> @lang('general.nav.back') </a>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form method="post" action="{{ route('headCategories.store') }}" data-parsley-validate class="form-horizontal form-label-left">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-sm-2 col-form-label col-xs-12" for="name">@lang('settingTypes.name') <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" value="{{ Request::old('name') ?: '' }}" id="name" name="name" class="form-control col-md-7 col-xs-12">
                                    @if ($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('default_id') ? ' has-error' : '' }}">
                                <label class="col-sm-2 col-form-label col-xs-12" for="default_id">@lang('general.app.set_as_default') </label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="checkbox checkbox-primary">
                                        <input id="checkbox" type="checkbox" name="default_id" value="1" @if (old('default_id') == 1)) checked="" @endif >
                                        <label for="checkbox"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12 col-md-offset-2">
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <button type="submit" class="btn btn-success">@lang('general.form.save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop