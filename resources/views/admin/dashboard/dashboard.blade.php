@extends('templates.admin.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @lang('general.app.welcome') {{Auth::user()->name}}!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
