<?php

namespace OptiCore\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use OptiCore\Models\Transactions;
use OptiCore\Models\User;

class OCRMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $transaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $receipts)
    {
        $this->user = $user;
        $this->receipts = $receipts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = $this->subject('OCR Mail EasyKas')->view('emails.ocr');

        $files = $this->receipts;

        if(count($files > 0)) {
            foreach($files as $file) {
                $message->attach($file->getRealPath(), array(
                        'as' => $file->getClientOriginalName(), // If you want you can chnage original name to custom name
                        'mime' => $file->getMimeType())
                );
            }
        }

        return $message;
    }
}
