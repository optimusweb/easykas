<?php

namespace OptiCore\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use OptiCore\Models\Transactions;
use OptiCore\Models\User;

class PDFMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $transaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($values)
    {
//        $this->user = $user;
        $this->values = $values;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $values = $this->values->input('values');
        $userId = $this->values->input('user_id');

        $t=1;
        foreach($values as $val) {
            $transactions = Transactions::with('users')
                ->with('receipts')
                ->with('transactionValues.bookingTypes')
                ->with('transactionValues.categories')
                ->where('user_id', '=', $userId)
                ->find($val);

            $user = User::with('headCategory.categories.bookingTypes')->find($userId);

            $params = [
                'transactions' => $transactions,
                'user' => $user,
            ];

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('admin.transactions.transactions_pdf', $params);

            $message = $this->subject('PDF Mail EasyKas')->view('emails.pdf');

            $date = Carbon::now()->format('d-m-Y');
            $message->attachData($pdf->output(), 'easykas_transactie_'.$t.'_'.$date.'.pdf');

            $t++;
        }

        return $message;
    }
}
