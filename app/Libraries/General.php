<?php

/**
 * User: Mehmet Tosun
 * Date: 12-1-2018
 */
namespace OptiCore\Libraries;

class General
{
    public static function checkEmpty($val)
    {
        if (empty($val)){
            return "-";
        }else{
            return $val;
        }
    }
}