<?php

namespace OptiCore\Http\Controllers\Admin;

use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
       	return view('admin.dashboard.dashboard');
    }
}
