<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\SettingTypes;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SettingTypesController extends Controller
{
    /**
     * Instantiate a new SettingTypesController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingTypes = SettingTypes::with('settings')->withoutTrashed()->get();

        $params = [
            'title' => 'SettingTypes Listing',
            'settingTypes' => $settingTypes,
        ];

        return view('admin.settingTypes.settingTypes_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'title' => 'Create Settingstype',
        ];

        return view('admin.settingTypes.settingTypes_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
        ]);

        // check if the filled name already exists and is not soft deleted
        if ($settingType = SettingTypes::withTrashed()->where('name', $request->input('name'))->first()) {
            SettingTypes::withTrashed()->find($settingType->id)->restore();
        }else {
            $settingType = SettingTypes::create([
                'code' => $request->input('code'),
                'name' => $request->input('name'),
            ]);
        }

        return redirect()->route('settingTypes.index')->with('success', trans('general.form.flash.created',['name' => $settingType->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\SettingTypes  $settingTypes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $settingType = SettingTypes::findOrFail($id);

            $params = [
                'title' => 'Wijzig Instellingstype',
                'settingType' => $settingType,
            ];

            return view('admin.settingTypes.settingTypes_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\SettingTypes  $settingTypes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $settingType = SettingTypes::findOrFail($id);

            $params = [
                'title' => 'Wijzig Instellingstype',
                'settingType' => $settingType,
            ];

            return view('admin.settingTypes.settingTypes_edit')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\SettingTypes  $settingTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $settingType = SettingTypes::findOrFail($id);

            $this->validate($request, [
                'name' => 'required',
            ]);

            $settingType->name = $request->input('name');

            $settingType->save();

            return redirect()->route('settingTypes.index')->with('success', trans('general.form.flash.updated',['name' => $settingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\SettingTypes  $settingTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $settingType = SettingTypes::findOrFail($id);

            $settingType->delete();

            return redirect()->route('settingTypes.index')->with('success', trans('general.form.flash.deleted',['name' => $settingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
