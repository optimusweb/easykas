<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\TransactionValues;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransactionValuesController extends Controller
{
    /**
     * Instantiate a new TransactionValuesController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:transactions');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\TransactionValues  $transactionValues
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionValues $transactionValues)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\TransactionValues  $transactionValues
     * @return \Illuminate\Http\Response
     */
    public function edit(TransactionValues $transactionValues)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\TransactionValues  $transactionValues
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionValues $transactionValues)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\TransactionValues  $transactionValues
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionValues $transactionValues)
    {
        //
    }
}
