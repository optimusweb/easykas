<?php

namespace OptiCore\Http\Controllers\Admin;

use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use OptiCore\Mail\OCRMail;
use OptiCore\Models\BookingTypes;
use OptiCore\Models\Category;
use OptiCore\Models\Receipts;
use OptiCore\Models\SettingTypes;
use OptiCore\Models\Transactions;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\TransactionValues;
use OptiCore\Models\User;

class TransactionsController extends Controller
{
    /**
     * Instantiate a new TransactionsController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:transactions');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transactions::with('users')
                                    ->with('receipts')
                                    ->with('transactionValues.bookingTypes')
                                    ->with('transactionValues.categories')
                                    ->get();

        $countItems = count(Transactions::onlyTrashed()->get());

        $params = [
            'title' => 'HeadCategories Listing',
            'transactions' => $transactions,
            'countItems' => $countItems,
            'trashedItems' => 0
        ];

        return view('admin.transactions.transactions_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $date = Carbon::now()->format('d-m-Y');
        if ($id == 0) {
            $users = User::withoutTrashed()->get();

            $params = [
                'id' => 0,
                'user' => null,
                'users' => $users,
                'date' => $date,
            ];
        }

        if ($id != 0) {
            $users = User::withoutTrashed()->get();
            $user = User::with('headCategory.categories.bookingTypes')->find($id);
            $user_settings = User::with('userSettings')->with('userSettingsData')->find($id);
            $settingTypes = SettingTypes::with('settings')->get();

            $params = [
                'title' => 'Aanmaken Mutatie',
                'id' => $id,
                'users' => $users,
                'user' => $user,
                'user_settings' => $user_settings,
                'settingTypes' => $settingTypes,
                'date' => $date,
            ];
        }

        return view('admin.transactions.transactions_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'transaction_date' => 'required'
        ]);

        $user_id = $request->input('user_id');
        $user = User::with('headCategory.categories.bookingTypes')->find($user_id);

        $transaction_date = Carbon::parse($request->input('transaction_date'))->format('Y-m-d');

        $transaction = Transactions::create([
            'user_id' => $user_id,
            'transaction_date' => $transaction_date,
        ]);

        // save each bookingtype
        foreach($user->headCategory()->first()->categories as $category) {
            if ($category->bookingTypes()->count() > 0) {
                foreach ($category->bookingTypes()->get() as $bookingType) {
                    $transActionValue = new TransactionValues;
                    $transActionValue->transaction_id = $transaction->id;
                    $transActionValue->booking_type_id = $bookingType->id;
                    $transActionValue->value = $request->input('bt_'.$bookingType->id);
                    $transActionValue->save();
                }
            }
        }

        // save each category
        foreach($user->headCategory()->first()->categories as $category) {
            $transActionValue = new TransactionValues;
            $transActionValue->transaction_id = $transaction->id;
            $transActionValue->category_id = $category->id;
            $transActionValue->value = $request->input('cat_'.$category->id);
            $transActionValue->save();
        }

        $user_settings = User::with('userSettings')->with('userSettingsData')->find($user_id);

        foreach ($user_settings->userSettingsData as $usd){
            if ($usd->setting_type_id == 1){ // 1 = max_receipts
                $max_days = $usd->value;
            }
        }

        $receipts = array();
        for($c=1;$c<=$max_days;$c++) {
            if (count($request->file('receipt'.$c)) > 0) {
                $curRequest = $request->file('receipt'.$c);
                $receipts[] = $curRequest;
                $bonnenId = BookingTypes::where('code', '=', 'bonnen')->first()->id;

                $filename = $transaction->id . '_' . $c . '_' . time() . '.' . $curRequest->getClientOriginalExtension();
                if(mkdir(public_path('/uploads/receipts/' . $transaction->id . '/', 777))) {
                    Image::make($curRequest)->save(public_path('/uploads/receipts/' . $transaction->id . '/' . $filename));

                    Receipts::create([
                        'transaction_id' => $transaction->id,
                        'booking_type_id' => $bonnenId,
                        'image' => $filename,
                        'value' => $request->input('receipt_val' . $c),
                        'description' => $request->input('receipt_desc' . $c),
                    ]);
                }
            }
        }

        // send mail with attachments to user
        \Mail::to($user->ocrmail)->send(new OCRMail($user, $receipts));

        return redirect()->route('transactions.index')->with('success', trans('general.form.flash.created',['name' => ""]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transactions = Transactions::with('users')
            ->with('receipts')
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->find($id);

        $user = User::with('headCategory.categories.bookingTypes')->find($transactions->user_id);

        $params = [
            'transactions' => $transactions,
            'user' => $user,
        ];

        return view('admin.transactions.transactions_show')->with($params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $date = Carbon::now()->format('d-m-Y');
//        if ($id == 0) {
//            $users = User::find($id);
//
//            $params = [
//                'id' => 0,
//                'user' => null,
//                'users' => $users,
//                'date' => $date,
//            ];
//        }

        if ($id != 0) {
            $transaction = Transactions::with('transactionValues')->find($id);
            $userId = $transaction->user_id;
//            $users = User::find($userId);
            $user = User::with('headCategory.categories.bookingTypes')->find($userId);
            $user_settings = User::with('userSettings')->with('userSettingsData')->find($userId);
            $settingTypes = SettingTypes::with('settings')->get();

            $params = [
                'title' => 'Bewerken transactie',
                'transaction' => $transaction,
                'id' => $id,
                'user' => $user,
                'user_settings' => $user_settings,
                'settingTypes' => $settingTypes,
                'date' => $date,
            ];
        }

        return view('admin.transactions.transactions_edit')->with($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd('update');
        $this->validate($request, [
            'user_id' => 'required',
            'transaction_date' => 'required'
        ]);

//        $category = Category::findOrFail($id);
//
//        $this->validate($request, [
//            'name' => 'required',
//        ]);
//
//        $category->name = $request->input('name');
//        $category->set_default = $request->input('default_id');
//        $category->use_receipts = $request->input('use_receipts');
//
//        $category->save();

        $user_id = $request->input('user_id');
        $user = User::with('headCategory.categories.bookingTypes')->find($user_id);

        $transaction_date = Carbon::parse($request->input('transaction_date'))->format('Y-m-d');

        $transaction = Transactions::with('transactionValues')->findOrFail($id);
        $transaction->transaction_date = $transaction_date;
        $transaction->save();

        // save each bookingtype
        foreach($transaction->transactionValues as $transValue) {
            $transActionValue = TransactionValues::find($transValue->id);
            $transActionValue->value = $request->input($transValue->id);
            $transActionValue->save();
        }

        $t = 0;

        if (count($request->receipt) > 0) {
            // create the transaction folder for this transaction
            File::makeDirectory(public_path('/uploads/receipts/'.$transaction->id), 0775, true);

            $bonnenId = BookingTypes::where('code', '=', 'bonnen')->first()->id;

            foreach ($request->receipt as $receipt) {
//                if ($request->receipt_val[$t]) {
                $imagedata = file_get_contents($receipt);
                $base64 = base64_encode($imagedata);
//                $image = Image::make(base64_decode($base64));
//                echo $image->response('jpg', 70);
//                    echo '<img src="data:image/jpeg;base64,'.$base64.'">';
//                    dd('plaatje');
//                    dd($base64->getClientOriginalExtension());
                $filename = $transaction->id . '_' . $t . '_' . time() . '.' . $receipt->getClientOriginalExtension();
                Image::make($base64)->resize(800, '')->save(public_path('/uploads/receipts/' . $transaction->id . '/' . $filename));

                Receipts::create([
                    'transaction_id' => $transaction->id,
                    'booking_type_id' => $bonnenId,
                    'image' => $filename,
                    'value' => $request->receipt_val[$t],
                    'description' => $request->receipt_descr[$t],
                ]);
//                }
                $t++;

                // send a welcome mail to the user
            }
//            \Mail::to($user->ocrmail)->send(new OCRMail($user, $transaction));
        }

        return redirect()->route('transactions.index')->with('success', trans('general.form.flash.updated',['name' => $transaction->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transactions)
    {
        //
    }

    /**
     * Create a PDF of the specified resource from storage.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        $transactions = Transactions::with('users')
            ->with('receipts')
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->find($id);

        $user = User::with('headCategory.categories.bookingTypes')->find($transactions->user_id);

        $params = [
            'transactions' => $transactions,
            'user' => $user,
        ];

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('admin.transactions.transactions_pdf', $params);
        return $pdf->download();
//        return $pdf->stream();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mutations()
    {
        $name = 'app_user';
        $users = User::with('roles')
            ->where('newRegistration', null)
            ->whereHas('roles', function ($q) use ($name) {
                $q->where('name', $name);
            })->get();

//        $transactions = Transactions::with('users')
//            ->with('receipts')
//            ->with('transactionValues.bookingTypes')
//            ->with('transactionValues.categories')
//            ->get();

        $countItems = count(Transactions::onlyTrashed()->get());

        $params = [
            'title' => 'HeadCategories Listing',
            'transactions' => null,//$transactions,
            'countItems' => $countItems,
            'trashedItems' => 0,
            'users' => $users,
            'userId' => null,
            'dateRanges' => null,
        ];

        return view('admin.mutations.mutations_list')->with($params);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getMutations(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'date_range' => 'required',
        ]);

        $userId = $request->input('user_id');
        $dateRanges = $request->input('date_range');

        // split the given date in start- and end date
        $dateRangeExpl = explode("/", $dateRanges);
        $startDate = Carbon::parse($dateRangeExpl[0])->format('Y-m-d');
        $endDate = Carbon::parse($dateRangeExpl[1])->format('Y-m-d');

        $transactions = Transactions::where('user_id', '=', $userId)
                                    ->with('transactionValues.bookingTypes')
                                    ->with('transactionValues.categories')
                                    ->with('receipts.bookingTypes')
                                    ->whereBetween('transaction_date', array($startDate, $endDate))
                                    ->get();
        $kassaldo_eind_id = BookingTypes::where('code', '=', 'kassaldo_eind')->get()->first()->id;

        $name = 'app_user';
        $users = User::with('roles')
            ->where('newRegistration', null)
            ->whereHas('roles', function ($q) use ($name) {
                $q->where('name', $name);
            })->get();

        $params = [
            'title' => 'Transacties',
            'transactions' => $transactions,
            'countItems' => 0,
            'trashedItems' => 0,
            'users' => $users,
            'userId' => $userId,
            'dateRanges' => $dateRanges,
            'startDate' => $dateRangeExpl[0],
            'endDate' => $dateRangeExpl[1],
            'kassaldo_eind_id' => $kassaldo_eind_id,
            'kassaldo_eind' => null,
        ];

        return view('admin.mutations.mutations_list')->with($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mutation_quarters()
    {
        $name = 'app_user';
        $users = User::with('roles')
            ->where('newRegistration', null)
            ->whereHas('roles', function ($q) use ($name) {
                $q->where('name', $name);
            })->get();

//        $transactions = Transactions::with('users')
//            ->with('receipts.bookingTypes')
//            ->with('transactionValues.bookingTypes')
//            ->with('transactionValues.categories')
//            ->get();

        $countItems = count(Transactions::onlyTrashed()->get());

        $params = [
            'title' => 'HeadCategories Listing',
            'transactions' => null,//$transactions,
            'countItems' => $countItems,
            'trashedItems' => 0,
            'users' => $users,
            'userId' => null,
            'dateRanges' => null,
        ];

        return view('admin.mutations.mutation_quarters_list')->with($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMutation_quarters(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'date_range' => 'required',
        ]);

        $userId = $request->input('user_id');
        $dateRanges = $request->input('date_range');

        // split the given date in start- and end date
        $dateRangeExpl = explode("/", $dateRanges);
        $startDate = Carbon::parse($dateRangeExpl[0])->format('Y-m-d');
        $endDate = Carbon::parse($dateRangeExpl[1])->format('Y-m-d');

        $transactions = Transactions::where('user_id', '=', $userId)
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->with('receipts.bookingTypes')
            ->whereBetween('transaction_date', array($startDate, $endDate))
            ->get();
        $kassaldo_eind_id = BookingTypes::where('code', '=', 'kassaldo_eind')->get()->first()->id;
        $kassaldo_begin_id = BookingTypes::where('code', '=', 'kassaldo_begin')->get()->first()->id;

        $ontvangsten = 0;
        $uitgaven = 0;
        $kassaldo_begin = 0;
        $first = 0;
        foreach($transactions as $transaction){
            foreach($transaction->transactionValues as $val){
                if ($val->booking_type_id == $kassaldo_begin_id && $first == 0){
                    $kassaldo_begin = $val->value;
                    $first = 1;
                }

                if ($val->category_id != null && (mb_strtolower(Category::where('name', '=', 'Omzet')->first()->id) == $val->category_id || mb_strtolower(Category::where('name', '=', 'Betaalmethode')->first()->id) == $val->category_id)) {
                    $ontvangsten += $val->value;
                }elseif($val->category_id != null && (mb_strtolower(Category::where('name', '=', 'Kosten')->first()->id) == $val->category_id || mb_strtolower(Category::where('name', '=', 'Opnemen/storten')->first()->id) == $val->category_id)){
                    $uitgaven += $val->value;
                }
            }
        }

        $name = 'app_user';
        $users = User::with('roles')
            ->where('newRegistration', null)
            ->whereHas('roles', function ($q) use ($name) {
                $q->where('name', $name);
            })->get();

        $params = [
            'title' => 'Transacties',
            'transactions' => $transactions,
            'countItems' => 0,
            'trashedItems' => 0,
            'users' => $users,
            'userId' => $userId,
            'dateRanges' => $dateRanges,
            'startDate' => $dateRangeExpl[0],
            'endDate' => $dateRangeExpl[1],
            'kassaldo_eind_id' => $kassaldo_eind_id,
            'kassaldo_eind' => null,
            'kassaldo_begin' => $kassaldo_begin,
            'ontvangsten' => $ontvangsten,
            'uitgaven' => $uitgaven,
            'omzet_id' => Category::where('name', '=', 'Omzet')->first()->id,
            'betaalmethode_id' => Category::where('name', '=', 'Betaalmethode')->first()->id,
            'kosten_id' => Category::where('name', '=', 'Kosten')->first()->id,
            'opnemen_storten_id' => Category::where('name', '=', 'Opnemen/storten')->first()->id,
        ];

        return view('admin.mutations.mutation_quarters_list')->with($params);
    }

}
