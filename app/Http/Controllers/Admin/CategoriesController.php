<?php

namespace OptiCore\Http\Controllers\Admin;

use Illuminate\Http\Request;
use OptiCore\Models\Categories;
use OptiCore\Models\Category;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\HeadCategory;
use OptiCore\Models\User;

class CategoriesController extends Controller
{
    /**
     * Instantiate a new CategoriesController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:categories');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_categories = User::with('headCategory.categories.bookingTypes.userBookingTypes')
            ->with('userSettings')->find(1);

        $categories = Category::all();
        $countItems = count(Category::onlyTrashed()->get());

        $params = [
            'title' => 'Categories Listing',
            'categories' => $categories,
            'countItems' => $countItems,
            'trashedItems' => 0
        ];

        return view('admin.categories.categories_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'title' => 'Create Category'
        ];

        return view('admin.categories.categories_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        // check if the filled name already exists and is not soft deleted
        if ($category = Category::withTrashed()->where('name', $request->input('name'))->first()) {
            Category::withTrashed()->find($category->id)->restore();

            // overwrite the old values with the new one
            $category->name = $request->input('name');
            $category->set_default = $request->input('default_id');
            $category->use_receipts = $request->input('use_receipts');
            $category->is_negative = $request->input('is_negative');

            $category->save();
        }else {
            $category = Category::create([
                'name' => $request->input('name'),
                'set_default' => $request->input('default_id'),
                'use_receipts' => $request->input('use_receipts'),
            ]);
        }

        return redirect()->route('categories.index')->with('success', trans('general.form.flash.created',['name' => $category->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $params = [
                'title' => 'Delete Category',
                'category' => $category,
            ];

            return view('admin.categories.categories_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $params = [
                'title' => 'Edit Category',
                'category' => $category,
            ];

            return view('admin.categories.categories_edit')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $this->validate($request, [
                'name' => 'required',
            ]);

            $category->name = $request->input('name');
            $category->set_default = $request->input('default_id');
            $category->use_receipts = $request->input('use_receipts');
            $category->is_negative = $request->input('is_negative');

            $category->save();

            return redirect()->route('categories.index')->with('success', trans('general.form.flash.updated',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $category->delete();

            return redirect()->route('categories.index')->with('success', trans('general.form.flash.deleted',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try
        {
            $category = Category::onlyTrashed()->findOrFail($id);

            $params = [
                'title' => 'Verwijder Categorie',
                'category' => $category,
            ];

            return view('admin.categories.categories_force_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function force_delete($id)
    {
        try
        {
            $category = Category::onlyTrashed()->findOrFail($id);

            $category->forceDelete();

            return redirect()->route('categories.deleted_categories')->with('success', trans('general.form.flash.deleted',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_deleted()
    {
        $categories = Category::onlyTrashed()->get();
        $countItems = count(Category::withoutTrashed()->get());

        $params = [
            'title' => 'Categories Listing',
            'categories' => $categories,
            'countItems' => $countItems,
            'trashedItems' => 1
        ];

        return view('admin.categories.categories_list')->with($params);
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        try
        {

            Category::withTrashed()->findOrFail($id)->restore();
            $category = Category::findOrFail($id);

            return redirect()->route('categories.deleted_categories')->with('success', trans('general.form.flash.activated',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function link()
    {
        $headCategories = HeadCategory::all();

        $params = [
            'title' => 'Categories Listing',
            'headCategories' => $headCategories,
        ];

        return view('admin.categories.categories_link_list')->with($params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_link($id)
    {
        try
        {
            $headCategory = HeadCategory::with('categories')->findOrFail($id);

            $params = [
                'title' => 'Delete Category',
                'headCategory' => $headCategory,
            ];

            return view('admin.categories.categories_delete_link')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_link()
    {
        $categories = Category::all();
        $headCategories = HeadCategory::with('categories')->get();

        $params = [
            'title' => 'Create Category Link',
            'categories' => $categories,
            'headCategories' => $headCategories,
        ];

        return view('admin.categories.categories_create_link')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_link(Request $request)
    {
        $this->validate($request, [
            'headCategory' => 'required',
            'category' => 'required',
        ]);

        $headCategory = new HeadCategory();
        $headCategory->id = $request->input('headCategory');

        // delete all the existing categories for this headcategory
        $headCategory->categories()->detach();

        foreach ($request->input('category') as $perm) {
            $headCategory->categories()->attach($perm);
        }

        return redirect()->route('categories.link')->with('success', trans('general.form.flash.created',['name' => ""]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_link($id)
    {
        try
        {
            $categories = Category::all();
            $headCategories = HeadCategory::all();
            $headCategory = HeadCategory::with('categories')->findOrFail($id);
            $headCategory_categories = $headCategory->categories;

            $params = [
                'title' => 'Create Category Link',
                'headCategories' => $headCategories,
                'headCategory' => $headCategory,
                'categories' => $categories,
                'headCategory_categories' => $headCategory_categories,
            ];

            return view('admin.categories.categories_edit_link')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_link(Request $request, $id)
    {
        try
        {
            $headCategory = HeadCategory::find($id);

            $this->validate($request, [
                'category' => 'required',
            ]);

            // delete all the existing categories for this headcategory
            $headCategory->categories()->detach();

            foreach ($request->input('category') as $perm) {
                $headCategory->categories()->attach($perm);
            }

            return redirect()->route('categories.link')->with('success', trans('general.form.flash.updated',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_link($id)
    {
        try
        {
            $headCategory = HeadCategory::find($id);

            // delete all the existing categories for this headcategory
            $headCategory->categories()->detach();

            return redirect()->route('categories.index')->with('success', trans('general.form.flash.deleted',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
