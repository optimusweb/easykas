<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\Settings;
use OptiCore\Models\SettingTypes;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SettingsController extends Controller
{
    /**
     * Instantiate a new SettingsController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingTypes = SettingTypes::withoutTrashed()->get();

        $params = [
            'title' => 'Settings Listing',
            'settingTypes' => $settingTypes,
        ];

        return view('admin.settings.settings_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settingTypes = SettingTypes::withoutTrashed()->get();

        $params = [
            'title' => 'SettingTypes Listing',
            'settingTypes' => $settingTypes,
        ];

        return view('admin.settings.settings_edit')->with($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        $settingTypes = SettingTypes::withoutTrashed()->get();

        foreach($settingTypes as $settingType) {
            $this->validate($request, [ $settingType->code => 'required|numeric']);
            $settings = Settings::where('setting_type_id', '=', $settingType->id)->first();
            if(empty($settings)){
                $settings = new Settings;
            }
            $settings->setting_type_id = $settingType->id;
            $settings->value = $request->input($settingType->code);
            $settings->push();
        }

        return redirect()->route('settings.index')->with('success', trans('general.form.flash.updated',['name' => '']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Settings $settings)
    {
        //
    }
}
