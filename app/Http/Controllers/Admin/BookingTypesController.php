<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\BookingTypes;
use OptiCore\Models\Category;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookingTypesController extends Controller
{
    /**
     * Instantiate a new BookingTypesController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:categories');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookingTypes = BookingTypes::with('categories')->get();
        $countItemsBookingTypes = count(BookingTypes::onlyTrashed()->get());

        $params = [
            'title' => 'BookingTypes Listing',
            'bookingTypes' => $bookingTypes,
            'countItems' => $countItemsBookingTypes,
            'trashedItems' => 0
        ];

        return view('admin.bookingTypes.bookingTypes_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::withoutTrashed()->get();

        $params = [
            'title' => 'Create BookingType',
            'categories' => $categories,
        ];

        return view('admin.bookingTypes.bookingTypes_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'description' => 'required',
            'gb_rek' => 'regex:/[0-9]+[\/]?[0-9]*/',
            'category_id' => 'required',
        ]);

        // check if the filled name already exists and is not soft deleted
        if ($bookingType = BookingTypes::withTrashed()->where('name', $request->input('name'))->first()) {
            BookingTypes::withTrashed()->find($bookingType->id)->restore();

            // overwrite the old values with the new one
            $bookingType->code = $request->input('code');
            $bookingType->name = $request->input('name');
            $bookingType->description = $request->input('description');
            $bookingType->gb_rek = $request->input('gb_rek');
            $bookingType->btw_code = $request->input('btw_code');
            $bookingType->set_default = $request->input('default_id');
            $bookingType->is_negative = $request->input('is_negative');

            $bookingType->save();

            $category = Category::find($request->input('category_id'));
            $category->bookingTypes()->attach($bookingType);
        }else {
            $bookingType = BookingTypes::create([
                'code' => $request->input('code'),
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'gb_rek' => $request->input('gb_rek'),
                'btw_code' => $request->input('btw_code'),
                'set_default' => $request->input('default_id'),
                'is_negative' => $request->input('is_negative'),
            ]);
            $category = Category::find($request->input('category_id'));
            $category->bookingTypes()->attach($bookingType);
        }

        return redirect()->route('bookingTypes.index')->with('success', trans('general.form.flash.created',['name' => $bookingType->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\BookingTypes  $bookingTypes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $bookingType = BookingTypes::findOrFail($id);

            $params = [
                'title' => 'Verwijder Instellingstype',
                'bookingType' => $bookingType,
            ];

            return view('admin.bookingTypes.bookingTypes_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\BookingTypes  $bookingTypes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $bookingType = BookingTypes::findOrFail($id);
            $categories = Category::withoutTrashed()->get();

            $params = [
                'title' => 'Wijzig Instellingstype',
                'bookingType' => $bookingType,
                'categories' => $categories,
            ];

            return view('admin.bookingTypes.bookingTypes_edit')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\BookingTypes  $bookingTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $bookingType = BookingTypes::findOrFail($id);

            $this->validate($request, [
                'code' => 'required',
                'name' => 'required',
                'description' => 'required',
                'gb_rek' => 'regex:/[0-9]+[\/]?[0-9]*/',
                'category_id' => 'required',
            ]);

            $bookingType->code = $request->input('code');
            $bookingType->name = $request->input('name');
            $bookingType->description = $request->input('description');
            $bookingType->gb_rek = $request->input('gb_rek');
            $bookingType->btw_code = $request->input('btw_code');
            $bookingType->set_default = $request->input('default_id');
            $bookingType->is_negative = $request->input('is_negative');

            $bookingType->save();

            $category = Category::find($request->input('category_id'));

            // delete all existing values for this bookingtype
            $bookingType->categories()->detach();
            // add new category value for this bookingtype
            $category->bookingTypes()->attach($bookingType->id);

            return redirect()->route('bookingTypes.index')->with('success', trans('general.form.flash.updated',['name' => $bookingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\BookingTypes  $bookingTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $bookingType = BookingTypes::findOrFail($id);

            $bookingType->delete();
            $bookingType->categories()->detach();

            return redirect()->route('bookingTypes.index')->with('success', trans('general.form.flash.deleted',['name' => $bookingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try
        {
            $bookingType = BookingTypes::onlyTrashed()->findOrFail($id);

            $params = [
                'title' => 'Verwijder Instellingstype',
                'bookingType' => $bookingType,
            ];

            return view('admin.bookingTypes.bookingTypes_force_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function force_delete($id)
    {
        try
        {
            $bookingType = BookingTypes::onlyTrashed()->findOrFail($id);

            $bookingType->forceDelete();

            return redirect()->route('bookingTypes.deleted_bookingtypes')->with('success', trans('general.form.flash.deleted',['name' => $bookingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_deleted()
    {
        $bookingTypes = BookingTypes::onlyTrashed()->get();

        $params = [
            'title' => 'BookingTypes Listing',
            'bookingTypes' => $bookingTypes,
            'trashedItems' => 1
        ];

        return view('admin.bookingTypes.bookingTypes_list')->with($params);
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\BookingTypes  $bookingTypes
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        try
        {

            BookingTypes::withTrashed()->findOrFail($id)->restore();
            $bookingType = BookingTypes::findOrFail($id);

            return redirect()->route('bookingTypes.deleted_bookingtypes')->with('success', trans('general.form.flash.activated',['name' => $bookingType->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function link()
    {
        $categories = Category::has('bookingTypes')->get();

        $params = [
            'title' => 'BookingTypes Listing',
            'categories' => $categories,
        ];

        return view('admin.bookingTypes.bookingTypes_link_list')->with($params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_link($id)
    {
        try
        {
            $category = Category::has('bookingTypes')->findOrFail($id);

            $params = [
                'title' => 'Delete Category',
                'category' => $category,
            ];

            return view('admin.bookingTypes.bookingTypes_delete_link')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_link()
    {
        $categories = Category::withoutTrashed()->get();
        $bookingTypes = bookingTypes::withoutTrashed()->get();
        $category_bookingTypes = Category::withoutTrashed()->has('bookingTypes')->get();

        $params = [
            'title' => 'Create Category Link',
            'categories' => $categories,
            'bookingTypes' => $bookingTypes,
            'category_bookingTypes' => $category_bookingTypes
        ];

        return view('admin.bookingTypes.bookingTypes_create_link')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_link(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'bookingType' => 'required',
        ]);

        $category = new Category();
        $category->id = $request->input('category');

        // delete all the existing categories for this headcategory
        $category->bookingTypes()->detach();

        foreach ($request->input('bookingType') as $perm) {
            $category->bookingTypes()->attach($perm);
        }

        return redirect()->route('bookingTypes.link')->with('success', trans('general.form.flash.created',['name' => ""]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_link($id)
    {
        try
        {
            $bookingTypes = BookingTypes::all();
            $category = Category::has('bookingTypes')->findOrFail($id);
            $category_bookingTypes = $category->bookingTypes;
//            dd($category_bookingTypes);

            $params = [
                'title' => 'Create Category Link',
                'category' => $category,
                'bookingTypes' => $bookingTypes,
                'category_bookingTypes' => $category_bookingTypes,
            ];

            return view('admin.bookingTypes.bookingTypes_edit_link')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_link(Request $request, $id)
    {
        try
        {
            $category = Category::find($id);

            $this->validate($request, [
                'bookingType' => 'required',
            ]);

            // delete all the existing categories for this headcategory
            $category->bookingTypes()->detach();

            foreach ($request->input('bookingType') as $perm) {
                $category->bookingTypes()->attach($perm);
            }

            return redirect()->route('bookingTypes.link')->with('success', trans('general.form.flash.updated',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_link($id)
    {
        try
        {
            $category = Category::find($id);

            // delete all the existing categories for this headcategory
            $category->bookingTypes()->detach();

            return redirect()->route('bookingTypes.link')->with('success', trans('general.form.flash.deleted',['name' => $category->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
