<?php

namespace OptiCore\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Image;
use OptiCore\Mail\Activation;
use OptiCore\Models\BookingTypes;
use OptiCore\Models\HeadCategory;
use OptiCore\Models\SettingTypes;
use OptiCore\Models\User;
use OptiCore\Models\Role;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\UserBookingTypes;
use OptiCore\Models\UserSettings;

class UsersController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = 'app_user';
        $users = User::where('newRegistration', null)
                        ->whereHas('roles', function ($q) use ($name) {
                            $q->where('name', '!=', $name);
                        })->get();

        $countItems = count(User::onlyTrashed()->get());

        $params = [
            'title' => 'Users Listing',
            'users' => $users,
            'countItems' => $countItems,
            'trashedItems' => 0,
            'app_user' => 0
        ];

        return view('admin.users.users_list')->with($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_app()
    {
        $name = 'app_user';
        $users = User::with('roles')
                        ->where('newRegistration', null)
                        ->whereHas('roles', function ($q) use ($name) {
                            $q->where('name', $name);
                        })->get();
        $countItems = count(User::onlyTrashed()->get());

        $params = [
            'title' => 'Users Listing',
            'users' => $users,
            'countItems' => $countItems,
            'trashedItems' => 0,
            'app_user' => 1
        ];

        return view('admin.users.users_app_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::withoutTrashed()->get();
        $headCategories = HeadCategory::withoutTrashed()->get();
        $settingTypes = SettingTypes::with('settings')->withoutTrashed()->get();

        $params = [
            'title' => 'Create User',
            'roles' => $roles,
            'headCategories' => $headCategories ,
            'settingTypes' => $settingTypes ,
        ];

        return view('admin.users.users_create')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_app_user()
    {
        $roles = Role::withoutTrashed()->get();
        $headCategories = HeadCategory::withoutTrashed()->get();
        $settingTypes = SettingTypes::with('settings')->withoutTrashed()->get();

        $params = [
            'title' => 'Create User',
            'roles' => $roles,
            'headCategories' => $headCategories ,
            'settingTypes' => $settingTypes ,
        ];

        return view('admin.users.users_app_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'role_id' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = User::create([
            'gender' => $request->input('gender'),
            'firstName' => $request->input('firstName'),
            'middleName' => $request->input('middleName'),
            'lastName' => $request->input('lastName'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'active' => 1,
        ]);

        // Save the role
        $role = Role::find($request->input('role_id'));

        $user->attachRole($role);


        return redirect()->route('users.index')->with('success', trans('general.form.flash.created',['name' => $user->name]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_app_user(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'companyName' => 'required',
            'role_id' => 'required',
            'head_category_id' => 'required',
            'ocrMail' => 'required|email|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        if ($request->hasFile('logo')){
            $logo = $request->file('logo');
            $filename = time(). '.' . $logo->getClientOriginalExtension();
            $data = getimagesize($logo);
            $width = $data[0];
            $height = $data[1];
            if($width > $height){
                $setWidth = "100px";
                $setHeight = "";
            }else{
                $setHeight = "100px";
                $setWidth = "";
            }
            Image::make($logo)->resize($setWidth, $setHeight)->save(public_path('/uploads/logos/' . $filename));
        }else{
            $filename = '';
        }

        $user = User::create([
            'gender' => $request->input('gender'),
            'firstName' => $request->input('firstName'),
            'middleName' => $request->input('middleName'),
            'lastName' => $request->input('lastName'),
            'companyName' => $request->input('companyName'),
            'street' => $request->input('street'),
            'houseNumber' => $request->input('houseNumber'),
            'zip' => $request->input('zip'),
            'city' => $request->input('city'),
            'ocrmail' => $request->input('ocrMail'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'logo' => $filename,
            'active' => 1,
        ]);

        // Save the role
        $role = Role::find($request->input('role_id'));

        $user->attachRole($role);

        // Save the headCategory
        $headCategory = HeadCategory::find($request->input('head_category_id'));

        $user->headCategory()->attach($headCategory);

        // Save the user settings
        $settingTypes = SettingTypes::withoutTrashed()->get();

        foreach($settingTypes as $settingType) {
            $userSettings = new UserSettings;
//            }
            $req_settingType = $request->input($settingType->code);
            if (!empty($req_settingType)){
                $userSettings->user_id = $user->id;
                $userSettings->setting_type_id = $settingType->id;
                $userSettings->value = $req_settingType;
                $userSettings->save();
            }
        }

        // Save the user bookingTypes
        $headCategory = HeadCategory::with('categories.bookingTypes')->find($request->input('head_category_id'));

        foreach($headCategory->categories as $category) {
            foreach($category->bookingTypes as $bookingType) {
                if (!empty($request->input('bt_name_'.$bookingType->id))){
                    $userBookingType = new UserBookingTypes();
                    $userBookingType->user_id = $user->id;
                    $userBookingType->booking_type_id = $bookingType->id;
                    $userBookingType->name = $request->input('bt_name_'.$bookingType->id);
                    $userBookingType->description = $request->input('bt_descr_'.$bookingType->id);
                    $userBookingType->gb_rek = $request->input('bt_gb_rek_'.$bookingType->id);
                    $userBookingType->save();
                }
            }
        }

        return redirect()->route('users.index_app')->with('success', trans('general.form.flash.created',['name' => $user->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $params = [
                'title' => 'Delete User',
                'user' => $user,
            ];

            return view('admin.users.users_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_app_user($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $params = [
                'title' => 'Delete User',
                'user' => $user,
            ];

            return view('admin.users.users_app_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $user = User::with('roles')->findOrFail($id);

            $roles = Role::all();

            $params = [
                'title' => 'Edit User',
                'user' => $user,
                'roles' => $roles,
            ];

            return view('admin.users.users_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_app_user($id)
    {
        try
        {
            $user = User::with('headCategory.categories.bookingTypes.userBookingTypes')->with('userSettings')->with('userSettingsData')->findOrFail($id);

            $headCategories = HeadCategory::withoutTrashed()->get();
            $settingTypes = SettingTypes::with('settings')->withoutTrashed()->get();

            $roles = Role::all();

            $params = [
                'title' => 'Edit User',
                'user' => $user,
                'roles' => $roles,
                'headCategories' => $headCategories,
                'settingTypes' => $settingTypes,
            ];

            return view('admin.users.users_app_edit')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $user = User::findOrFail($id);

            $this->validate($request, [
                'firstName' => 'required',
                'lastName' => 'required',
                'role_id' => 'required',
                'email' => 'required|email|unique:users,email,'.$id,
            ]);

            $user->gender = $request->input('gender');
            $user->firstName = $request->input('firstName');
            $user->middleName = $request->input('middleName');
            $user->lastName = $request->input('lastName');
            $user->email = $request->input('email');

            $user->save();

            // Delete all existing roles for this user
            $roles = $user->roles;

            foreach ($roles as $key => $value) {
                $user->detachRole($value);
            }

            // Save the role
            $role = Role::find($request->input('role_id'));

            $user->attachRole($role);

            return redirect()->route('users.index')->with('success', trans('general.form.flash.updated',['name' => $user->name]));
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_app_user(Request $request, $id)
    {
        try
        {
            $user = User::findOrFail($id);

            $this->validate($request, [
                'firstName' => 'required',
                'lastName' => 'required',
                'companyName' => 'required',
                'role_id' => 'required',
                'head_category_id' => 'required',
                'ocrMail' => 'required|email|unique:users,ocrmail,'.$id,
                'email' => 'required|email|unique:users,email,'.$id,
            ]);

            $user->gender = $request->input('gender');
            $user->firstName = $request->input('firstName');
            $user->middleName = $request->input('middleName');
            $user->lastName = $request->input('lastName');
            $user->companyName = $request->input('companyName');
            $user->street = $request->input('street');
            $user->houseNumber = $request->input('houseNumber');
            $user->zip = $request->input('zip');
            $user->city = $request->input('city');
            $user->ocrmail = $request->input('ocrMail');
            $user->email = $request->input('email');

            if ($request->hasFile('logo')){
                $logo = $request->file('logo');
                $filename = time(). '.' . $logo->getClientOriginalExtension();
                $data = getimagesize($logo);
                $width = $data[0];
                $height = $data[1];
                if($width > $height){
                    $setWidth = "100px";
                    $setHeight = "";
                }else{
                    $setHeight = "100px";
                    $setWidth = "";
                }
                Image::make($logo)->resize($setWidth, $setHeight)->save(public_path('/uploads/logos/' . $filename));
                $user->logo = $filename;
            }

            $user->save();

            // Delete all existing roles for this user
            $roles = $user->roles;

            foreach ($roles as $key => $value) {
                $user->detachRole($value);
            }

            // Save the role
            $role = Role::find($request->input('role_id'));

            $user->attachRole($role);

            // Delete all existing categories for this user
            $headCategories = $user->headCategory;
            if (count($headCategories) > 0) {
//                foreach ($headCategories as $key => $value) {
                    $user->headCategory()->detach($headCategories);
//                }
            }

            // Save the headCategory
            $headCategory = HeadCategory::find($request->input('head_category_id'));

            $user->headCategory()->attach($headCategory);

            // Save the user settings
            $settingTypes = SettingTypes::withoutTrashed()->get();

            foreach($settingTypes as $settingType) {
                $userSettings = UserSettings::where('setting_type_id', '=', $settingType->id)->where('user_id', '=', $user->id)->first();
                if(empty($userSettings)){
                    $userSettings = new UserSettings;
                }
                $req_settingType = $request->input($settingType->code);
                if (!empty($req_settingType)){
                    $userSettings->user_id = $user->id;
                    $userSettings->setting_type_id = $settingType->id;
                    $userSettings->value = $req_settingType;
                    $userSettings->save();
                }
            }
            // Save or update the userBookingTypes
            $headCategory = HeadCategory::with('categories.bookingTypes')->find($request->input('head_category_id'));
            $userBookingTypes = UserBookingTypes::where('user_id', 9)->get();

            foreach($headCategory->categories as $category) {
                foreach($category->bookingTypes as $bookingType) {
                    $set_ubt = 0;
                    $ubt_result = $userBookingTypes->where('booking_type_id', '=', $bookingType->id)->where('user_id', '=', $user->id)->first();

                    if(isset($ubt_result) && empty($request->input('bt_name_' . $bookingType->id)) &&
                        empty($request->input('bt_descr_' . $bookingType->id)) &&
                        empty($request->input('bt_gb_rek_' . $bookingType->id))){
                        $ubt_result->forceDelete();
                    }elseif (isset($ubt_result)) {
                        $userBookingType = UserBookingTypes::find($ubt_result->id);
                        $set_ubt = 1;
                    }elseif(!empty($request->input('bt_name_' . $bookingType->id))) {
                        $userBookingType = new UserBookingTypes();
                        $set_ubt = 1;
                    }

                    if ($set_ubt == 1) {
                        $userBookingType->user_id = $user->id;
                        $userBookingType->booking_type_id = $bookingType->id;
                        $userBookingType->name = $request->input('bt_name_' . $bookingType->id);
                        $userBookingType->description = $request->input('bt_descr_' . $bookingType->id);
                        $userBookingType->gb_rek = $request->input('bt_gb_rek_' . $bookingType->id);
                        $userBookingType->save();
                    }
                }
            }

            return redirect()->route('users.index_app')->with('success', trans('general.form.flash.updated',['name' => $user->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        $id = \Auth::user()->id;

        $user = User::findOrFail($id);

        $params = [
            'title' => 'Change Password',
            'user' => $user,
        ];

        return view('admin.users.users_changepass')->with($params);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function updatePassword(Request $request){
        try
        {
            $user = User::findOrFail(Auth::id());

            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
            ]);

            $user->password = bcrypt($request->input('password'));
            $user->save();

            return redirect()->route('users.index')->with('success', trans('general.form.flash.updated',['name' => $user->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $user->delete();

            return redirect()->route('users.index')->with('success', trans('general.form.flash.deleted',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_app_user($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $user->delete();

            return redirect()->route('users.index_app')->with('success', trans('general.form.flash.deleted',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function newRegistrations(){
        $users = User::all()->where('newRegistration', '1');

        $params = [
            'title' => 'Users Listing',
            'users' => $users,
        ];

        return view('admin.users.users_new_registrations')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNewRegistration(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'companyName' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = User::create([
            'firstname' => $request->input('firstName'),
            'middlename' => $request->input('middleName'),
            'lastname' => $request->input('lastName'),
            'companyname' => $request->input('companyName'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        $role = Role::find($request->input('role_id'));

        $user->attachRole($role);

        return redirect()->route('users.index')->with('success', trans('general.form.flash.created',['name' => $user->name]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try
        {
            $user = User::onlyTrashed()->findOrFail($id);

            $params = [
                'title' => 'Verwijder Gebruiker',
                'user' => $user,
            ];

            return view('admin.users.users_force_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function force_delete($id)
    {
        try
        {
            $user = User::onlyTrashed()->findOrFail($id);

            $user->forceDelete();

            return redirect()->route('users.deleted_users')->with('success', trans('general.form.flash.deleted',['name' => $user->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_app_user($id)
    {
        try
        {
            $user = User::onlyTrashed()->findOrFail($id);

            $params = [
                'title' => 'Verwijder Gebruiker',
                'user' => $user,
            ];

            return view('admin.users.users_app_force_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function force_delete_app_user($id)
    {
        try
        {
            $user = User::onlyTrashed()->findOrFail($id);

            $user->forceDelete();

            return redirect()->route('users.deleted_app_users')->with('success', trans('general.form.flash.deleted',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_deleted()
    {
        $users = User::onlyTrashed()->get();
        $countItems = count(User::withoutTrashed()->get());

        $params = [
            'title' => 'Users Listing',
            'users' => $users,
            'countItems' => $countItems,
            'trashedItems' => 1
        ];

        return view('admin.users.users_list')->with($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_app_users_deleted()
    {
        $users = User::onlyTrashed()->get();
        $countItems = count(User::withoutTrashed()->get());

        $params = [
            'title' => 'Users Listing',
            'users' => $users,
            'countItems' => $countItems,
            'trashedItems' => 1
        ];

        return view('admin.users.users_app_list')->with($params);
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        try
        {
            User::withTrashed()->findOrFail($id)->restore();
            $user = User::findOrFail($id);

            // Save the role
            $role = Role::where('name', '=', 'default')->first();

            $user->attachRole($role->id);

            return redirect()->route('users.deleted_users')->with('success', trans('general.form.flash.activated',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function activate_app_user($id)
    {
        try
        {
            User::withTrashed()->findOrFail($id)->restore();
            $user = User::findOrFail($id);
            // Save the role
            $role = Role::where('name', '=', 'app_user')->first();

            $user->attachRole($role->id);

            return redirect()->route('users.deleted_app_users')->with('success', trans('general.form.flash.activated',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function activateNewUser($id)
    {
        try
        {
            $user = User::findOrFail($id);
            $user->newRegistration = null;
            $user->active = 1;
            $user->save();

            // send activation mail to user
            \Mail::to($user)->send(new Activation($user));

            return redirect()->route('users.edit_app_user', ['id' => $id])->with('success', trans('general.form.flash.activated',['name' => $user->firstName]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
