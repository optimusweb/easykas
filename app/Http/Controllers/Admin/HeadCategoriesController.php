<?php

namespace OptiCore\Http\Controllers\Admin;

use Illuminate\Http\Request;
use OptiCore\Models\HeadCategory;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HeadCategoriesController extends Controller
{
    /**
     * Instantiate a new CategoriesController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:categories');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $headCategories = HeadCategory::all();
        $countItems = count(HeadCategory::onlyTrashed()->get());

        $params = [
            'title' => 'HeadCategories Listing',
            'headCategories' => $headCategories,
            'countItems' => $countItems,
            'trashedItems' => 0
        ];

        return view('admin.headCategories.headCategories_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'title' => 'Create Head categorie',
        ];

        return view('admin.headCategories.headCategories_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        // check if the filled name already exists and is not soft deleted
        if ($headCategory = HeadCategory::withTrashed()->where('name', $request->input('name'))->first()) {
            HeadCategory::withTrashed()->find($headCategory->id)->restore();

            // overwrite the old values with the new one
            $headCategory->name = $request->input('name');
            $headCategory->set_default = $request->input('default_id');

            $headCategory->save();
        }else {
            $headCategory = HeadCategory::create([
                'name' => $request->input('name'),
                'set_default' => $request->input('default_id'),
            ]);
        }

        return redirect()->route('headCategories.index')->with('success', trans('general.form.flash.created',['name' => $headCategory->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\HeadCategories  $headCategories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $headCategory = HeadCategory::findOrFail($id);

            $params = [
                'title' => 'Wijzig Instellingstype',
                'headCategory' => $headCategory,
            ];

            return view('admin.headCategories.headCategories_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\HeadCategories  $headCategories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $headCategory = HeadCategory::findOrFail($id);

            $params = [
                'title' => 'Wijzig Instellingstype',
                'headCategory' => $headCategory,
            ];

            return view('admin.headCategories.headCategories_edit')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\HeadCategories  $headCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $headCategory = HeadCategory::findOrFail($id);

            $this->validate($request, [
                'name' => 'required',
            ]);

            $headCategory->name = $request->input('name');
            $headCategory->set_default = $request->input('default_id');

            $headCategory->save();

            return redirect()->route('headCategories.index')->with('success', trans('general.form.flash.updated',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\HeadCategories  $headCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $headCategory = HeadCategory::findOrFail($id);

            $headCategory->delete();
            $headCategory->categories()->detach();

            return redirect()->route('headCategories.index')->with('success', trans('general.form.flash.deleted',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try
        {
            $headCategory = HeadCategory::onlyTrashed()->findOrFail($id);

            $params = [
                'title' => 'Verwijder Hoofdcategorie',
                'headCategory' => $headCategory,
            ];

            return view('admin.headCategories.headCategories_force_delete')->with($params);
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function force_delete($id)
    {
        try
        {
            $headCategory = HeadCategory::onlyTrashed()->findOrFail($id);

            $headCategory->forceDelete();

            return redirect()->route('headCategories.deleted_headcategories')->with('success', trans('general.form.flash.deleted',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_deleted()
    {
        $headCategories = HeadCategory::onlyTrashed()->get();
        $countItems = count(HeadCategory::withoutTrashed()->get());

        $params = [
            'title' => 'HeadCategories Listing',
            'headCategories' => $headCategories,
            'countItems' => $countItems,
            'trashedItems' => 1
        ];

        return view('admin.headCategories.headCategories_list')->with($params);
    }

    /**
     * Activate the specified resource.
     *
     * @param  \OptiCore\HeadCategories  $headCategories
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        try
        {
            HeadCategory::withTrashed()->findOrFail($id)->restore();
            $headCategory = HeadCategory::findOrFail($id);

            return redirect()->route('headCategories.deleted_headcategories')->with('success', trans('general.form.flash.activated',['name' => $headCategory->name]));
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

}
