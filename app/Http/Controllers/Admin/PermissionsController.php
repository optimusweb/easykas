<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\Role;
use OptiCore\Models\Permission;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PermissionsController extends Controller
{
    /**
     * Instantiate a new PermissionsController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();

        $params = [
            'title' => 'Permissies Lijst',
            'permissions' => $permissions,
        ];

        return view('admin.permissions.permissions_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        $params = [
            'title' => 'Create Permission',
            'permissions' => $permissions,
        ];
        
        return view('admin.permissions.permissions_create')->with($params);   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions',
            'display_name' => 'required|unique:permissions,display_name,',
            'description' => 'required',
        ]);
        
        $permission = Permission::create([
            'name' => strtolower($request->input('name')),
            'display_name' => $request->input('display_name'),
            'description' => $request->input('description'),
        ]);
        
        return redirect()->route('permissions.index')->with('success', trans('general.form.flash.created',['name' => $permission->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $permission = Permission::findOrFail($id);

            $params = [
            'title' => 'Delete permission',
            'permission' => $permission,
            ];

            return view('admin.permissions.permissions_delete')->with($params);  
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $permission = Permission::findOrFail($id);

            $params = [
                'title' => 'Edit permission',
                'permission' => $permission
            ];

            return view('admin.permissions.permissions_edit')->with($params);
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $this->validate($request, [
                'name' => 'required|unique:permissions,name,'.$id,
                'display_name' => 'required|unique:permissions,display_name,',
                'description' => 'required',
            ]);
            
            $permission = Permission::findOrFail($id);
            
            $permission->name = strtolower($request->input('name'));
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');

            $permission->save();

            return redirect()->route('permissions.index')->with('success', trans('general.form.flash.updated',['name' => $permission->name]));
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $permission = Permission::findOrFail($id);

            $permission->delete();

            return redirect()->route('permissions.index')->with('success', trans('general.form.flash.deleted',['name' => $permission->name]));
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
