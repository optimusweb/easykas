<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\Permission;
use OptiCore\Models\Role;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('permissions')->get();

        $params = [
            'title' => 'Rollen Lijst',
            'roles' => $roles,
        ];

        return view('admin.roles.roles_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all();
        $permissions = Permission::all();

        $params = [
            'title' => 'Create Role',
            'permissions' => $permissions,
            'role' => $role,
        ];
        
        return view('admin.roles.roles_create')->with($params);   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',
            'display_name' => 'required',
            'description' => 'required',
            'permissions' => 'required',
        ]);
        
        $role = Role::create([
            'name' => strtolower($request->input('name')),
            'display_name' => $request->input('display_name'),
            'description' => $request->input('description'),
        ]);

        // save the existing permissions for this role
        if (count($request->input('permissions') > 0)) {
            foreach ($request->input('permissions') as $perm) {
                $role->permissions()->attach($perm);
            }
        }

        return redirect()->route('roles.index')->with('success', trans('general.form.flash.created',['name' => $role->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $role = Role::findOrFail($id);

            $params = [
            'title' => 'Delete role',
            'role' => $role,
            ];

            return view('admin.roles.roles_delete')->with($params);  
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $role = Role::findOrFail($id);
            $permissions = Permission::all();

            $params = [
                'title' => 'Edit role',
                'role' => $role,
                'permissions' => $permissions,
            ];

            return view('admin.roles.roles_edit')->with($params);
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $this->validate($request, [
                'name' => 'required|unique:roles,name,'.$id,
                'display_name' => 'required',
                'description' => 'required',
                'permissions' => 'required',
            ]);
            
            $role = Role::findOrFail($id);
            
            $role->name = strtolower($request->input('name'));
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');

            // delete all the existing permissions for this role             
            $role->permissions()->detach();

            // save the existing permissions for this role
            foreach ($request->input('permissions') as $perm) {
                $role->permissions()->attach($perm);
            }

            $role->save();

            return redirect()->route('roles.index')->with('success', trans('general.form.flash.updated',['name' => $role->name]));
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $role = Role::findOrFail($id);

            $role->delete();

            return redirect()->route('roles.index')->with('success', trans('general.form.flash.deleted',['name' => $role->name]));
        }
        
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
}
