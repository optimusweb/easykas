<?php

namespace OptiCore\Http\Controllers\Admin;

use OptiCore\Models\Information;
use Illuminate\Http\Request;
use OptiCore\Http\Controllers\Controller;

class InformationController extends Controller
{
    /**
     * Instantiate a new InformationController instance.
     */
    public function __construct()
    {
        $this->middleware('permission:settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $information = Information::first();

        $params = [
            'title' => 'Informatie',
            'information' => $information,
        ];

        return view('admin.information.information_show')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \OptiCore\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OptiCore\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = Information::find($id);

        $params = [
            'title' => 'Informatie',
            'information' => $information,
            'id' => $id
        ];

        return view('admin.information.information_edit')->with($params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $pages = array(1,2);

            if (in_array($id, $pages)) {
                $information = Information::find($id);

                $this->validate($request, [
                    'information_text' => 'required',
                ]);

                if (empty($information)) {
                    $information = Information::create([
                        'text' => $request->input('information_text'),
                    ]);
                } else {
                    $information->text = $request->input('information_text');

                    $information->save();
                }

                if ($id == 1) {
                    return redirect()->route('information.index')->with('success', trans('general.form.flash.updated', ['name' => 'Informatie tekst']));
                } else {
                    return redirect()->route('information.information_page')->with('success', trans('general.form.flash.updated', ['name' => 'Informatie tekst']));
                }
            }else{
                return response()->view('errors.'.'404');
            }
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OptiCore\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function info_page()
    {
        $information = Information::find(2);

        $params = [
            'title' => 'Informatie',
            'information' => $information,
        ];

        return view('admin.information.information_page_show')->with($params);
    }

}
