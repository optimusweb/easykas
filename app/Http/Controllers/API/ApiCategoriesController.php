<?php

namespace OptiCore\Http\Controllers\API;

use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use OptiCore\Models\BookingTypes;
use OptiCore\Models\Categories;
use OptiCore\Models\Category;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\HeadCategory;
use OptiCore\Models\Transactions;
use OptiCore\Models\TransactionValues;
use OptiCore\Models\User;
use Illuminate\Support\Facades\File;
use OptiCore\Models\Receipts;

class ApiCategoriesController extends Controller
{
    /**
     * Instantiate a new CategoriesController instance.
     */
    public function __construct()
    {
//        $this->middleware('permission:categories');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories($id)
    {
//        $id = auth()->user()->id;
        // temporary user ugur.kucuk
//        $id = 2;
        $user_categories = User::with('headCategory.categories.bookingTypes.userBookingTypes')
                                ->with('userSettingsData.settingType')->find($id);

        $params = [
            'user_categories' => $user_categories
        ];

        return response()->json($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories_tree($id)
    {
        $categories_tree = User::with('headCategory.categories.bookingTypes.userBookingTypes')->find($id);

        $params = [
            'categories_tree' => $categories_tree
        ];

        return response()->json($params);
    }

}
