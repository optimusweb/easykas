<?php

namespace OptiCore\Http\Controllers\API;

use Illuminate\Support\Facades\App;
use Image;
use Carbon\Carbon;
use OptiCore\Mail\PDFMail;
use OptiCore\Models\Information;
use Illuminate\Http\Request;
use OptiCore\Models\BookingTypes;
use OptiCore\Models\Categories;
use OptiCore\Models\Category;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\HeadCategory;
use OptiCore\Models\Transactions;
use OptiCore\Models\TransactionValues;
use OptiCore\Models\User;
use Illuminate\Support\Facades\File;
use OptiCore\Models\Receipts;
use OptiCore\Models\UserSettings;

class ApiTransactionsController extends Controller
{
    /**
     * Instantiate a new CategoriesController instance.
     */
    public function __construct()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $this->validate($request, [
                'user_id' => 'required',
                'transaction_date' => 'required'
            ]);

            $user_id = $request->input('user_id');
            $user = User::with('headCategory.categories.bookingTypes')->find($user_id);

            $transaction_date = Carbon::parse($request->input('transaction_date'))->format('Y-m-d');

            $transaction = Transactions::create([
                'user_id' => $user_id,
                'transaction_date' => $transaction_date,
            ]);

            if (count($user->headCategory()->first()->categories) > 0) {
                // save each bookingtype
                foreach ($user->headCategory()->first()->categories as $category) {
                    if ($category->bookingTypes()->count() > 0) {
                        foreach ($category->bookingTypes()->get() as $bookingType) {
                            $transActionValue = new TransactionValues;
                            $transActionValue->transaction_id = $transaction->id;
                            $transActionValue->booking_type_id = $bookingType->id;
                            $transActionValue->value = $request->input('bt_' . $bookingType->id);
                            $transActionValue->save();
                        }
                    }
                }

                // save each category
                foreach ($user->headCategory()->first()->categories as $category) {
                    $transActionValue = new TransactionValues;
                    $transActionValue->transaction_id = $transaction->id;
                    $transActionValue->category_id = $category->id;
                    $transActionValue->value = $request->input('cat_' . $category->id);
                    $transActionValue->save();
                }
            }

            $user_settings = User::with('userSettings')->with('userSettingsData')->find($user_id);

            foreach ($user_settings->userSettingsData as $usd){
                if ($usd->setting_type_id == 1){ // 1 = max_receipts
                    $max_days = $usd->value;
                }
            }

            $receipts = array();
            for($c=0;$c<=$max_days;$c++) {
                if (count($request->file('receipt'.$c)) > 0) {
                    $curRequest = $request->file('receipt'.$c);
                    $receipts[] = $curRequest;
                    $bonnenId = BookingTypes::where('code', '=', 'bonnen')->first()->id;

                    $filename = $transaction->id . '_' . $c . '_' . time() . '.' . $curRequest->getClientOriginalExtension();
                    if(mkdir(public_path('/uploads/receipts/' . $transaction->id . '/', 777))) {
                        Image::make($curRequest)->save(public_path('/uploads/receipts/' . $transaction->id . '/' . $filename));

                        Receipts::create([
                            'transaction_id' => $transaction->id,
                            'booking_type_id' => $bonnenId,
                            'image' => $filename,
                            'value' => $request->input('receipt_val' . $c),
                            'description' => $request->input('receipt_desc' . $c),
                        ]);
                    }else{
                        return response()->json("Could not create folder");
                    }
                }
            }

            return response()->json("success");
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->json("failed");
            }
        }
//        return redirect()->route('categories.index')->with('success', trans('general.form.flash.created',['name' => $category->name]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $this->validate($request, [
                'user_id' => 'required',
                'transaction_date' => 'required'
            ]);

            $user_id = $request->input('user_id');
            $user = User::with('headCategory.categories.bookingTypes')->find($user_id);

            $transaction_date = Carbon::parse($request->input('transaction_date'))->format('Y-m-d');

            $transaction = Transactions::with('transactionValues')->findOrFail($id);
            $transaction->transaction_date = $transaction_date;
            $transaction->save();

            // save each bookingtype
            foreach($transaction->transactionValues as $transValue) {
                $transActionValue = TransactionValues::find($transValue->id);
                $transActionValue->value = $request->input($transValue->id);
                $transActionValue->save();
            }

            $user_settings = User::with('userSettings')->with('userSettingsData')->find($user_id);

            foreach ($user_settings->userSettingsData as $usd){
                if ($usd->setting_type_id == 1){ // 1 = max_receipts
                    $max_days = $usd->value;
                }
            }

            $receipts = array();
            for($c=0;$c<=$max_days;$c++) {
                if (count($request->file('receipt'.$c)) > 0) {
                    $curRequest = $request->file('receipt'.$c);
                    $receipts[] = $curRequest;
                    $bonnenId = BookingTypes::where('code', '=', 'bonnen')->first()->id;

                    $filename = $transaction->id . '_' . $c . '_' . time() . '.' . $curRequest->getClientOriginalExtension();
                    Image::make($curRequest)->save(public_path('/uploads/receipts/' . $transaction->id . '/' . $filename));

                    Receipts::create([
                        'transaction_id' => $transaction->id,
                        'booking_type_id' => $bonnenId,
                        'image' => $filename,
                        'value' => $request->input('receipt_val'.$c),
                        'description' => $request->input('receipt_desc'.$c),
                    ]);
                }
            }

            // send mail with attachments to user
//            \Mail::to($user->ocrmail)->send(new OCRMail($user, $receipts));

            return response()->json("success");
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->json("failed");
            }
        }
    }

    /**
     * get the Transactions for user.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function getUserTransactions($id, $from, $to){
        $fromDate = Carbon::parse($from)->format('Y-m-d');
        $toDate = Carbon::parse($to)->format('Y-m-d');

        $transactions = Transactions::with('users')
            ->with('receipts')
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->where('user_id', '=', $id)
            ->whereBetween('transaction_date', array($fromDate, $toDate))
            ->get();

        $params = [
            'transactions' => $transactions
        ];

        return response()->json($params);
    }

    /**
     * get the give Transaction.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function getTransaction($id){
        $transaction = Transactions::with('users')
            ->with('receipts')
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->where('id', '=', $id)
            ->where('transaction_date', '>', Carbon::now()->subDays(2))
            ->get();

        $params = [
            'transaction' => $transaction
        ];

        return response()->json($params);
    }

    /**
     * Create a PDF of the specified resource from storage.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        try
        {
            $transactions = Transactions::with('users')
                ->with('receipts')
                ->with('transactionValues.bookingTypes')
                ->with('transactionValues.categories')
                ->find($id);

            $user = User::with('headCategory.categories.bookingTypes')->find($transactions->user_id);

            $params = [
                'transactions' => $transactions,
                'user' => $user,
            ];

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('admin.transactions.transactions_pdf', $params);
            return $pdf->download();
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->json("failed");
            }
        }
    }

    /**
     * Create a PDF of the specified resource from storage and sent it as attachment in the mail to the user.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function mailPdf(Request $request)
    {
        try
        {
            $userId = $request->input('user_id');

            $userMail = User::find($userId)->email;

            // send mail with attachments to user
            \Mail::to($userMail)->send(new PDFMail($request));

            return response()->json("email successfully sent");
        }
        catch (ModelNotFoundException $ex)
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->json("failed to send mail");
            }
        }
    }

    /**
     * Delete a receipt from the database.
     *
     * @param  \OptiCore\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy_receipt($id){
        $receipt = Receipts::find($id);

        // delete the receipt in the database
        if ($receipt->forceDelete()) {
            // unlink the file in the upload folder
            unlink(public_path('/uploads/receipts/' . $receipt->transaction_id . '/' . $receipt->image));
        }
    }

}
