<?php
/**
 * Created by PhpStorm.
 * User: MEMBU
 * Date: 29-12-2017
 * Time: 15:23
 */

namespace OptiCore\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use OptiCore\Http\Controllers\Controller;
use OptiCore\Mail\Welcome;
use OptiCore\Models\BookingTypes;
use OptiCore\Models\Transactions;
use OptiCore\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class PassportController extends Controller
{

    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password'), 'active' => 1])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['id'] =  $user->id;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['firstName'] = $request->input('name');
        $input['password'] = bcrypt($input['password']);
        $input['newRegistration'] = 1;
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        // send a welcome mail to the user
        \Mail::to($user)->send(new Welcome($user));

        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function getDetails()
    {
        $user = Auth::user();

        $transactions = Transactions::where('user_id', '=', $user->id)
            ->with('transactionValues.bookingTypes')
            ->with('transactionValues.categories')
            ->with('receipts.bookingTypes')
            ->orderBy('transaction_date', 'desc')
            ->latest()->get();

        $kassaldo_eind_id = BookingTypes::where('code', '=', 'kassaldo_eind')->get()->first()->id;

        $first = 0;
        foreach($transactions as $transaction){
            foreach($transaction->transactionValues as $val){
                if ($val->booking_type_id == $kassaldo_eind_id && $first == 0){
                    $kassaldo_begin = $val->value;
                    $first = 1;
                }

            }
        }

        // set the beginsaldo in the user array to return
        $user['beginsaldo'] = $kassaldo_begin;

        return response()->json(['success' => $user], $this->successStatus);
    }
}