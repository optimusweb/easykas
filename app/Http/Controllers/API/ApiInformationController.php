<?php

namespace OptiCore\Http\Controllers\API;

use Illuminate\Http\Request;
use OptiCore\Models\Categories;
use OptiCore\Models\Category;
use OptiCore\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use OptiCore\Models\HeadCategory;
use OptiCore\Models\Information;
use OptiCore\Models\User;

class ApiInformationController extends Controller
{
    /**
     * Instantiate a new CategoriesController instance.
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function information_icon()
    {
        $information = Information::find(1);

        $params = [
            'information' => $information
        ];

        return response()->json($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function information_page()
    {
        $information = Information::find(2);

        $params = [
            'information' => $information
        ];

        return response()->json($params);
    }

}
