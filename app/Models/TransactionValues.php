<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionValues extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'booking_type_id',
        'category_id',
        'value',
    ];

    public function bookingTypes()
    {
        return $this->belongsTo('OptiCore\Models\BookingTypes','booking_type_id');
    }

    public function categories()
    {
        return $this->belongsTo('OptiCore\Models\Category','category_id');
    }
}
