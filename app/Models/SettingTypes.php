<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingTypes extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
    ];

    public function settings()
    {
        return $this->hasOne('OptiCore\Models\Settings','setting_type_id');
    }

    public function userSettings()
    {
        return $this->hasOne('OptiCore\Models\UserSettings','setting_type_id');
    }
}
