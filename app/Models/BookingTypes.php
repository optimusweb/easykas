<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingTypes extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'name',
        'description',
        'gb_rek',
        'btw_code',
        'set_default',
    ];

//    public function transactionValues()
//    {
//        return $this->belongsToMany('OptiCore\Models\TransactionValues','transaction_values','booking_type_id','id');
//    }

    public function categories()
    {
        return $this->belongsToMany('OptiCore\Models\Category','categories_booking_types','booking_type_id','category_id');
//        return $this->belongsTo('OptiCore\Models\Category');
    }

    public function userBookingTypes()
    {
        return $this->hasMany('OptiCore\Models\UserBookingTypes', 'booking_type_id');
//        return $this->hasOne('OptiCore\Models\UserBookingTypes','booking_type_id');
    }

    public function receipt()
    {
        return $this->hasOne('OptiCore\Models\Receipts','booking_type_id');
    }
}
