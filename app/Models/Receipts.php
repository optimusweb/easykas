<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipts extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'booking_type_id',
        'image',
        'value',
        'description',
    ];

    public function bookingTypes()
    {
        return $this->hasOne('OptiCore\Models\BookingTypes','id', 'booking_type_id');
    }
}
