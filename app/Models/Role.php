<?php

namespace OptiCore\Models;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends EntrustRole
{
	use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    /**
     * Get the brand that the product belongs to.
     */
    public function permissions()
    {
        // return $this->belongsToMany('OptiCore\Models\Permission', 'permission_id');
        return $this->belongsToMany('OptiCore\Models\Permission','permission_role','role_id','permission_id');
    }
}
