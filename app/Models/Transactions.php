<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transactions extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'transaction_date',
    ];

    public function transactionValues()
    {
        return $this->hasMany('OptiCore\Models\TransactionValues', 'transaction_id');
    }

    public function receipts()
    {
        return $this->hasMany('OptiCore\Models\Receipts', 'transaction_id');
    }

    public function users()
    {
        return $this->belongsTo('OptiCore\Models\User','user_id');
    }
}
