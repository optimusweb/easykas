<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBookingTypes extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'booking_type_id',
        'name',
        'description',
        'gb_rek',
    ];

    public function bookingTypes()
    {
        return $this->belongsToMany('OptiCore\Models\BookingTypes','user_booking_types','booking_type_id','id');
    }

}
