<?php

namespace OptiCore\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OptiCore\Notifications\OptiCoreAdminResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'gender',
        'firstName',
        'middleName',
        'lastName',
        'companyName',
        'street',
        'houseNumber',
        'suffix',
        'zip',
        'city',
        'lat',
        'long',
        'logo',
        'ocrmail',
        'newRegistration',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    public function getLogoUrl()
    {
        if (!empty($this->logo)) {
            return "/uploads/logos/" . $this->logo;
        }else{
            return false;
        }
    }

    public function role() 
    {
        return $this->hasOne('OptiCore\Models\Role', 'id', 'role_id');
    }

    public function headCategory()
    {
        return $this->belongsToMany('OptiCore\Models\HeadCategory', 'user_head_categories', 'user_id', 'head_category_id');
    }

    public function userSettings()
    {
        return $this->belongsToMany('OptiCore\Models\SettingTypes', 'user_settings', 'user_id', 'setting_type_id');
    }

    public function userSettingsData()
    {
        return $this->hasMany('OptiCore\Models\UserSettings', 'user_id');
    }

    public function transactions()
    {
        return $this->belongsToMany('OptiCore\Models\Transactions', 'user_id');
    }
}
