<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSettings extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'setting_type_id',
        'value',
        'endDate',
    ];

    public function user()
    {
        return $this->belongsTo('OptiCore\Models\User');
    }

    public function settingType()
    {
        return $this->belongsTo('OptiCore\Models\SettingTypes');
    }
}
