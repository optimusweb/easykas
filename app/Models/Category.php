<?php

namespace OptiCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'set_default',
    ];

    public function bookingTypes()
    {
        return $this->belongsToMany('OptiCore\Models\BookingTypes','categories_booking_types','category_id','booking_type_id');
    }

}
