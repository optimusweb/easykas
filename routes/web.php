<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'login',
    ])->middleware('auth');

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]], function(){
    Route::group(['prefix' => 'admin','middleware' => 'auth','namespace' => 'Admin'],function(){
        Route::resource('home', 'DashboardController');

        // Categories routes
        Route::get('categories/link', 'CategoriesController@link')->name('categories.link');
        Route::get('categories/create_link', 'CategoriesController@create_link')->name('categories.create_link');
        Route::post('categories/store_link', 'CategoriesController@store_link')->name('categories.store_link');
        Route::get('categories/edit_link/{id}', 'CategoriesController@edit_link')->name('categories.edit_link');
        Route::post('categories/update_link/{id}', 'CategoriesController@update_link')->name('categories.update_link');
        Route::get('categories/show_link/{id}', 'CategoriesController@show_link')->name('categories.show_link');
        Route::post('categories/delete_link/{id}', 'CategoriesController@destroy_link')->name('categories.destroy_link');
        Route::get('categories/activate/{id}', 'CategoriesController@activate')->name('categories.activate');
        Route::get('categories/delete/{id}', 'CategoriesController@delete')->name('categories.delete');
        Route::post('categories/force_delete/{id}', 'CategoriesController@force_delete')->name('categories.force_delete');
        Route::get('deleted_categories', 'CategoriesController@list_deleted')->name('categories.deleted_categories');
        Route::resource('categories', 'CategoriesController');

        // Headcategories routes
        Route::get('headCategories/activate/{id}', 'HeadCategoriesController@activate')->name('headCategories.activate');
        Route::get('headCategories/delete/{id}', 'HeadCategoriesController@delete')->name('headCategories.delete');
        Route::post('headCategories/force_delete/{id}', 'HeadCategoriesController@force_delete')->name('headCategories.force_delete');
        Route::get('deleted_headcategories', 'HeadCategoriesController@list_deleted')->name('headCategories.deleted_headcategories');
        Route::resource('headCategories', 'HeadCategoriesController');

        // BookingTypes routes
        Route::get('bookingTypes/activate/{id}', 'BookingTypesController@activate')->name('bookingTypes.activate');
        Route::get('bookingTypes/link', 'BookingTypesController@link')->name('bookingTypes.link');
        Route::get('bookingTypes/create_link', 'BookingTypesController@create_link')->name('bookingTypes.create_link');
        Route::post('bookingTypes/store_link', 'BookingTypesController@store_link')->name('bookingTypes.store_link');
        Route::get('bookingTypes/edit_link/{id}', 'BookingTypesController@edit_link')->name('bookingTypes.edit_link');
        Route::post('bookingTypes/update_link/{id}', 'BookingTypesController@update_link')->name('bookingTypes.update_link');
        Route::get('bookingTypes/show_link/{id}', 'BookingTypesController@show_link')->name('bookingTypes.show_link');
        Route::post('bookingTypes/destroy_link/{id}', 'BookingTypesController@destroy_link')->name('bookingTypes.destroy_link');
        Route::get('bookingTypes/activate/{id}', 'BookingTypesController@activate')->name('bookingTypes.activate');
        Route::get('bookingTypes/delete/{id}', 'BookingTypesController@delete')->name('bookingTypes.delete');
        Route::post('bookingTypes/force_delete/{id}', 'BookingTypesController@force_delete')->name('bookingTypes.force_delete');
        Route::get('deleted_bookingtypes', 'BookingTypesController@list_deleted')->name('bookingTypes.deleted_bookingtypes');
        Route::resource('bookingTypes', 'BookingTypesController', ['except' => 'delete_link']);

        // settings routes
        Route::resource('settings', 'SettingsController');
        Route::resource('settingTypes', 'SettingTypesController');

        // transactions / mutations routes
        Route::get('transactions/create/{id}', 'TransactionsController@create')->name('transactions.create');
        Route::get('transactions/show/{id}', 'TransactionsController@show')->name('transactions.show');
        Route::get('transactions/pdf/{id}', 'TransactionsController@pdf')->name('transactions.pdf');
        Route::get('mutation_quarters', 'TransactionsController@mutation_quarters')->name('transactions.mutation_quarters');
        Route::post('mutation_quarters', 'TransactionsController@getMutation_quarters')->name('transactions.get_mutation_quarters');
        Route::get('mutations', 'TransactionsController@mutations')->name('transactions.mutations');
        Route::post('mutations', 'TransactionsController@getMutations')->name('transactions.get_mutations');
        Route::resource('transactions', 'TransactionsController', ['except' => 'create']);

        // users, roles and permissions routes
        Route::get('users/activate/{id}', 'UsersController@activate')->name('users.activate');
        Route::get('users/activate_app_user/{id}', 'UsersController@activate_app_user')->name('users.activate_app_user');
        Route::get('users_app', 'UsersController@index_app')->name('users.index_app');
        Route::get('users/users_app/{id}', 'UsersController@show_app')->name('users.show_app');
        Route::get('create_app_user', 'UsersController@create_app_user')->name('users.create_app_user');
        Route::get('users_app/{id}/edit', 'UsersController@edit_app_user')->name('users.edit_app_user');
        Route::post('users/store_app_user', 'UsersController@store_app_user')->name('users.store_app_user');
        Route::get('users/delete/{id}', 'UsersController@delete')->name('users.delete');
        Route::get('users/delete_app_user/{id}', 'UsersController@delete_app_user')->name('users.delete_app_user');
        Route::get('users_app/{id}', 'UsersController@show_app_user')->name('users.show_app');
        Route::post('users_app/{id}', 'UsersController@destroy_app_user')->name('users.destroy_app');
        Route::post('users/update_app_user/{id}', 'UsersController@update_app_user')->name('users.update_app_user');
        Route::post('users/force_delete/{id}', 'UsersController@force_delete')->name('users.force_delete');
        Route::post('users/force_delete_app_user/{id}', 'UsersController@force_delete_app_user')->name('users.force_delete_app_user');
        Route::get('deleted_users', 'UsersController@list_deleted')->name('users.deleted_users');
        Route::get('deleted_app_users', 'UsersController@list_app_users_deleted')->name('users.deleted_app_users');
        Route::resource('users', 'UsersController');
        Route::resource('roles', 'RolesController');
        Route::resource('permissions', 'PermissionsController');

        // Information page routes
        Route::resource('information', 'InformationController');
        Route::get('information_page', 'InformationController@info_page')->name('information.information_page');
    });

    // User routes
    $this->get('admin/new_registrations', 'Admin\UsersController@newRegistrations')->name('users.newRegistrations');
    $this->get('admin/activate_new_user/{id}', 'Admin\UsersController@activateNewUser')->name('users.activateNewUser');

    // Change password routes
    $this->get('admin/changepass', 'Admin\UsersController@changePassword')->name('changepass')->middleware('auth');
    $this->post('admin/changepass', 'Admin\UsersController@updatePassword');

    // Authentication Routes...
    $this->get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('admin/login', 'Auth\LoginController@login');
    $this->get('admin/logout', 'Auth\LoginController@logout')->name('logout');

    // Password Reset Routes...
    $this->get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    $this->post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
    $this->post('admin/password/reset', 'Auth\ResetPasswordController@reset');
});