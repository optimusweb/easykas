<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('login', 'API\PassportController@login');
Route::post('register', 'API\PassportController@register');

Route::get('categories/{id}', 'API\ApiCategoriesController@categories');
Route::get('categories_tree/{id}', 'API\ApiCategoriesController@categories_tree');
Route::post('save_transaction', 'API\ApiTransactionsController@store');
Route::post('update_transaction/{id}', 'API\ApiTransactionsController@update');
Route::get('user_transactions/{id}/{fromDate}/{toDate}', 'API\ApiTransactionsController@getUserTransactions');
Route::get('transaction/{id}', 'API\ApiTransactionsController@getTransaction');
Route::get('transactions/pdf/{id}', 'API\ApiTransactionsController@pdf');
Route::post('transactions/mail_pdf', 'API\ApiTransactionsController@mailPdf');
Route::get('delete_receipt/{id}', 'API\ApiTransactionsController@destroy_receipt');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('get_details', 'API\PassportController@getDetails');
    Route::get('information_icon', 'API\ApiInformationController@information_icon');
    Route::get('information_page', 'API\ApiInformationController@information_page');
});