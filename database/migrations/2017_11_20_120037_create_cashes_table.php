<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->integer('100')->nullable();
            $table->integer('50')->nullable();
            $table->integer('20')->nullable();
            $table->integer('10')->nullable();
            $table->integer('5')->nullable();
            $table->integer('2')->nullable();
            $table->integer('1')->nullable();
            $table->integer('05')->nullable();
            $table->integer('02')->nullable();
            $table->integer('01')->nullable();
            $table->integer('005')->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->decimal('start', 10, 2)->nullable();
            $table->decimal('counted', 10, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('transaction_id')->references('id')->on('transactions')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash');
    }
}
