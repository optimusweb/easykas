<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHeadCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_head_categories', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('head_category_id')->unsigned();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('head_category_id')->references('id')->on('head_categories')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'head_category_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('head_category_id');
    }
}
