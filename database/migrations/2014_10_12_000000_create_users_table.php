<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('gender')->nullable();
            $table->string('firstName');
            $table->string('middleName')->nullable();
            $table->string('lastName');
            $table->string('companyName');
            $table->string('street')->nullable();
            $table->string('houseNumber')->nullable();
            $table->string('suffix')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->binary('logo')->nullable();
            $table->string('ocrmail')->unique()->nullable();
            $table->boolean('newRegistration')->nullable();
            $table->boolean('active')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
