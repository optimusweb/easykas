<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriesBookingTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_booking_types', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('booking_type_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('booking_type_id')->references('id')->on('booking_types')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['category_id', 'booking_type_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_booking_types');
    }
}
