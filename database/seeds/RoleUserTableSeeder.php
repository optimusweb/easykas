<?php

use OptiCore\Models\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'user_id' => 1,
                'role_id' => 1
            ],
            [
                'user_id' => 2,
                'role_id' => 1
            ]
        ];

        $user = new User();
        // save the existing permissions for this role
        $user->roles()->attach($roles);
    }
}