<?php

use Illuminate\Database\Seeder;
use OptiCore\Models\SettingTypes;

class SettingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settingTypes = [
            [
                'code' => 'max_receipts',
                'name' => 'Maximaal aantal bonnen',
            ],
            [
                'code' => 'days_for_change',
                'name' => 'Aantal dagen om te kunnen wijzigen',
            ]
        ];

        foreach ($settingTypes as $key => $value) {
            SettingTypes::create($value);
        }
    }
}
