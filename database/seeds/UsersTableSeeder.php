<?php

use OptiCore\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'mehmet.tosun@optimusweb.nl',
                'password' => '$2y$10$8smYa5YkRWDD8.b1tbFzoehlJnqWy0qNPZd6sJIw.mZnPHqnmDhiS',
                'gender' => 'M',
                'firstName' => 'Mehmet',
                'lastName' => 'Tosun',
                'companyName' => 'Optimus Websolutions',
                'active' => '1',
            ],
            [
                'email' => 'ugur.kucuk@optimusweb.nl',
                'password' => '$2y$10$8smYa5YkRWDD8.b1tbFzoehlJnqWy0qNPZd6sJIw.mZnPHqnmDhiS',
                'firstName' => 'Ugur',
                'lastName' => 'Kucuk',
                'companyName' => 'Optimus Websolutions',
                'active' => '1',
            ]
        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }
    }
}