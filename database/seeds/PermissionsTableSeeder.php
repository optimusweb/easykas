<?php

use OptiCore\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name' => 'create',
                'display_name' => 'Voeg item toe',
                'description' => 'Toestemming om items toe te voegen'
            ],
            [
                'name' => 'edit',
                'display_name' => 'Bewerk item',
                'description' => 'Toestemming om items te bewerken'
            ],
            [
                'name' => 'delete',
                'display_name' => 'Verwijder item',
                'description' => 'Toestemming om items te verwijderen'
            ],
            [
                'name' => 'users',
                'display_name' => 'Beheren gebruikers',
                'description' => 'Toestemming om gebruikers te beheren'
            ],
            [
                'name' => 'transactions',
                'display_name' => 'Beheren transacties',
                'description' => 'Toestemming om transacties te beheren'
            ],
            [
                'name' => 'categories',
                'display_name' => 'Beheren categorieen',
                'description' => 'Toestemming om categorieen te beheren'
            ],
            [
                'name' => 'settings',
                'display_name' => 'Beheren instellingen',
                'description' => 'Toestemming om instellingen te beheren'
            ],
            [
                'name' => 'overviews',
                'display_name' => 'Beheren overzichten',
                'description' => 'Toestemming om overzichten te beheren'
            ]
        ];

        foreach ($permission as $key => $value) {
            Permission::create($value);
        }
    }
}
