<?php

use Illuminate\Database\Seeder;
use OptiCore\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'permission_id' => 1,
                'role_id' => 1
            ],
            [
                'permission_id' => 2,
                'role_id' => 1
            ],
            [
                'permission_id' => 3,
                'role_id' => 1
            ],
            [
                'permission_id' => 4,
                'role_id' => 1
            ],
            [
                'permission_id' => 1,
                'role_id' => 2
            ],
            [
                'permission_id' => 2,
                'role_id' => 2
            ]
        ];

        $role = new Role();
        // save the existing permissions for this role
        $role->permissions()->attach($permissions);
    }
}
