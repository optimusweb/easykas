<?php

use OptiCore\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administrator',
                'description' => 'Rechten tot alle functionaliteit'
            ],
            [
                'name' => 'manager',
                'display_name' => 'Manager',
                'description' => 'Beheerder met beperkte rechten'
            ],
            [
                'name' => 'default',
                'display_name' => 'Standaard',
                'description' => 'Alleen toevoegen'
            ]
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
    }
}